Feature: Automating Entry Form of OASIS - Discharge from Agency

Scenario: Automating Entry Form of Demographics / Clinical Record
    Given I Login
    And I visit patient admitted page
    And I click discharge tab
    When I search the patient
    And I click the added patient
    And I click OASIS - Discharge from Agency
    And I click Edit Button

    # DEMOGRAPHICS/CLINICAL RECORD TAB Start -------------
    And I input time in and time out
    And I input date assessment completed
    And I selecting 1 Medicare traditional fee for service for Current Payment Sources for Home Care
    And I selecting 1 for Influenza Vaccine Data Collection Period
    And I selecting 1 for Influenza Vaccine Received
    And I click No for Pneumococcal Vaccine
    And I click 3 for Reason Pneumococcal Vaccine not received
    And I click save button
    # DEMOGRAPHICS/CLINICAL RECORD TAB End -------------

    Scenario: Automating Entry Form of Vital Signs / Sensory Tab
    Given I Login1
    And I visit patient admitted page
    And I click discharge tab
    When I search the patient
    And I click the added patient
    And I click OASIS - Discharge from Agency
    And I click Edit Button
    And I click Vital Signs or Sensory Tab

    # VITAL SIGNS/SENSORY TAB Start -------------
    And I input temperature and click oral
    And I input pulse or HR and click radial
    And I input respiration
    And I input systolic and diastolic for BP left arm and clicking sitting
    And I input systolic and diastolic for BP right arm and clicking sitting
    And I input systolic and diastolic for BP left leg and clicking sitting
    And I input systolic and diastolic for BP right leg and clicking sitting
    And I input O2 Saturation % on room air
    And I input O2 Saturation % on O2 and input LPM
    And I input blood sugar and clicking fbs
    And I click actual for weight and height
    And Clicking yes for evidence for infection and input description
    And I click MD and CM or Supervisior
    And Clicking yes for NewChangeHold medications and input description
    And I input observation and intervention
    And Clicking 1 for frequency pain
    # Location 1
    And I input pain location
    And I select type
    And I select present level
    And I select worst level
    And I select acceptable level
    And I select level after meds
    #Location2
    And I input pain location1
    And I select type1
    And I select present level1
    And I select worst level1
    And I select acceptable level1
    And I select level after meds1

    And I select all character pain
    And I select all non verbal signs of pain
    And I select all what makes the pain better
    And I select all what makes the pain worse
    And I click yes for pain mediaction and input description for profile
    And I click daily for how often pain meds needed
    And I click effective pain medication effectiveness
    And I click yes for physician aware of pain
    And I input observation and intervention1
    # Sensory Status
    # Eyes
    And I click left and right for cataract
    And I click left and right for glaucoma
    And I click left and right for redness
    And I click left and right for pain
    And I click left and right for itching
    And I click left and right for ptsosis
    And I click left and right for sclera reddened
    And I click left and right for edema of eyelids
    And I click left and right for blurred vision
    And I click left and right for blind
    And I click left and right for exudate from eyes
    And I click left and right for excessive tearing
    And I click left and right for macular degeneration
    And I click left and right for retinopathy
    And I input other and click left and right for other status
    # Ears
    And I click left and right for HOH
    And I click left and right for Hearing aid
    And I click left and right for Deaf
    And I click left and right for Tinnitus
    And I click left and right for Drainage
    And I click left and right for Pain for ears
    And I input other and click vertigo
    # Mouth
    And I click dentures and click upper, lower and partial
    And Clicking poor dentition, gingivitis, toothache and loss of taste for Mouth
    And I click lesions and input description for lesion
    And I click mass or tumor and input description for mass or tumor
    And I click difficulty of chewing or swallowing
    And I input others for mouth
    # Nose
    And I select all for nose
    And I input lesions for nose
    And I input others for nose
    # Throat
    And I select all for throat
    And I input lesions for throat
    And I input others for throat
    # Speech
    And I select all for Speech
    And I input others for Speech
    # Touch
    And I select all for Touch
    And I input others for Touch

    And I input observation and intervention2
    And I click save button

Scenario: Automating Entry Form of Integumentary / Endocrine Tab
    Given I Login2
    And I visit patient admitted page
    And I click discharge tab
    When I search the patient
    And I click the added patient
    And I click OASIS - Discharge from Agency
    And I click Edit Button
    And I click Integumentary or Endocrine Tab

    # INTEGUMENTARY/ENDOCRINE TAB Start -------------
    And I click pink for skin color and input other
    And I click warm for skin temp
    And I click dry for moisture and input other
    And I click normal for turgor

    #Skin Integrity
    And I click skin intact, lesion and add lesion icon
    And I select one lesion
    And I input location and comment
    And I click wound and add wound icon
    And I input wound, location and comment
    And I input other for skin integrity
    And I input observation and intervention

    #Braden Scale for Predicting Pressure Sore Risk
    And I click 1 for sensory perception
    And I click 1 for moisture
    And I click 1 for activity
    And I click 1 for mobility
    And I click 1 for nutrition
    And I click 2 for friction and shear
    And I click yes for M1306
    And I click 1 for M1307
    #(M1311) - Current Number of Unhealed Pressure Ulcers/Injuries at Each Stage
    And I input A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1 and F2
    #(M1324) - Stage of Most Problematic Unhealed Pressure Ulcer/Injury that is Stageable
    And I click 2 for M1324
    #(M1330) - Does this patient have a Stasis Ulcer?
    And I click 2 for M1330
    #(M1332) - Current Number of Stasis Ulcer(s) that are Observable
    And I click 2 for M1334
    #(M1340) - Does this patient have a Surgical Wound?
    And I click 1 for M1340
    #(M1342) - Status of Most Problematic Surgical Wound that is Observable
    And I click 2 for M1342

    #Endocrine System
    And I select all for endocrine system
    #Diabetes
    And I input Hgb A1C and date tested
    And I click new for onset
    And I input onset date
    #Diabetes management (Select All)
    And I select all for diabetes management
    And I click yes for Signs of hypoglycemia and hyperglycemia
    And I input observation and intervention1
    #Foot Assessment
    And I click left and right for Thick or ingrown toenail
    And I click left and right for Calluses or fissures
    And I click left and right for Interdigital macerations
    And I click left and right for Signs of fungal infection
    And I click left and right for Absent pedal pulses
    And I click left and right for Hot, red, swollen foot
    And I click left and right for Foot deformity -hammer or claw toes
    And I click left and right for Limited range of motion of joints
    And I click left and right for Decreased circulation - cold foot
    And I click left and right for Burning or tingling sensation, numbness
    And I click left and right for Loss of sensation to heat or cold
    And I input other and click left and right for other assessment
    And I click daily and input other for foot exam frequency
    And I select all for regular done by
    #Patient/Caregiver Competence
    And I click yes for patient and caregiver on Competent with glucometer use including control testing
    And I click yes for patient and caregiver on Competent with insulin preparation and administration
    And I click yes for patient and caregiver on Competent after instructions given and performed return demo
    And I click yes for patient and caregiver on Level of knowledge of disease process and management
    #Blood glucose testing
    And I click Before breakfast and input other for testing frequency
    And I input Brand or Model for Glucometer or CGM
    And I click control testing done
    #Other Form of Glucometer/CGM (Control Test Result)
    And I input level, mg or dl and clicking yes for within range
    And I input level, mg or dl and clicking yes for within range1
    And I select for reason testing
    And I input action taken
    And I click save button
    And modal will display
    And I click access later button
    # INTEGUMENTARY/ENDOCRINE TAB End -------------

Scenario: Automating Entry Form of Cardiopulmonary Tab
    Given I Login3
    And I visit patient admitted page
    And I click discharge tab
    When I search the patient
    And I click the added patient
    And I click OASIS - Discharge from Agency
    And I click Edit Button
    And I click Cardiopulmonary Tab

    # CARDIOPULMONARY TAB Start -------------
    And I click 1 for M1400
    And I click No for No Breath sounds clear, bilateral
    And I click left, right, anterior, posterior, upper, middle, lower for diminished
    And I click left, right, anterior, posterior, upper, middle, lower for absent
    And I click left, right, anterior, posterior, upper, middle, lower for rales crackles
    And I click left, right, anterior, posterior, upper, middle, lower for rhonchi
    And I click left, right, anterior, posterior, upper, middle, lower for wheeze
    And I click left, right, anterior, posterior, upper, middle, lower for stridor
    And I click yes for abnormal breathing patterns
    #Abnormal breathing patterns
    And I select all abnormal breathing patterns
    #Cough
    And I click yes for cough
    And I click productive for character
    And I click white for sputum color
    And I click thin for sputum character
    And I click small for sputum amount
    #Special Procedure (Select All)
    And I select all special procedure
    And I input observation and intervention
    #Oxygen Risk Assessment (All clicking yes)
    And I click yes for oxygen risk assessment
    #Are there potential sources of open flames identified? (select all)
    And I click all potential sources of open flames
    And I input observation and intervention1
    #Oxygen Theraphy
    And I click continuous for type
    #Oxygen delivery (select all)
    And I select all oxygen delivery
    And I input liters or minute
    #Oxygen source (select all)
    And I selecy all oxygen source
    And I click yes for backup O2 tank
    And I click vendor notified
    And I input observation and intervention2
    #Other form for Tracheostomy
    And I inpput brand, trach tube change, date last changed and inner cannula
    And I input observation and intervention3
    #Other form for BiPAP/CPAP
    And I input brand for BiPAP or CPAP
    And I click yes for Device working properly and Compliant with use of device
    And I input observation and intervention4
    #Other form for Suctioning
    And I click oral
    And I click yes for Is the person performing the suctioning proficient
    And I click yes for Is the suction equipment setup always ready for use
    #Suction procedure done by (select all)
    And I select all for suction procedure done by
    And I input observation and intervention5
    #Other form for Ventilator
    And I input brand, ridal volume, FiO2 and assist control for ventilator
    And I input PEEP, SIMV, pressure control and PRVC for ventilator
    And I input observation and intervention6
    #Other form for PleurX
    And I input date catheter inserted
    And I click daily and input other for drainage frequency
    And I input ml for amount drained
    And I click done
    #Procedure done by (select all)
    And I select all for procedure done by
    And I input observation and intervention7
    #Cardiovascular
    And I click regular for heart rhythm
    And I click least than three for capillary refill
    And I click yes for JVD, peripheral edema, chest pain and cardiac device
    And I select cardiac device
    And I click yes and input other fow weight gain
    #Pulses
    And I click pedal left and right for bounding
    And I click popliteal left and right for bounding
    And I click femoral left and right for bounding
    And I click brachial left and right for bounding
    And I click radial left and right for bounding
    And I input observation and intervention8
    #Peripheral edema
    And Clicking +1 for Pedal edema - Left
    And Clicking +1 for Pedal edema - Right
    And Clicking +1 for Ankle edema - Left
    And Clicking +1 for Ankle edema - Right
    And Clicking +1 for Leg edema - Left
    And Clicking +1 for Leg edema - Right
    And Clicking +1 for Sacral edema	
    And Clicking +1 for Generalized edema
    And I input observation and intervention9
    #Chest Pain
    #Character (select all)
    And I select all character
    And I click left for radiating to shoulder, jaw, neck, arm
    And I select all for accompanied by
    And I input frequency and duration of pain
    And I input observation and intervention10
    #Pace Maker
    And I input brand, rate setting, date implanted and date last tested
    And I click no for
    And I input observation and intervention11
    And I click save button
    # CARDIOPULMANRY TAB End -------------

Scenario: Automating Entry Form of Nutrition/Elimination Tab
    Given I Login4
    And I visit patient admitted page
    And I click discharge tab
    When I search the patient
    And I click the added patient
    And I click OASIS - Discharge from Agency
    And I click Edit Button
    And I click Nutrition or Elimination Tab

    # NUTRITION / ELIMINATION TAB Start -------------
    And I click soft and input inches in girt for abdomen
    And I click active for bowel sounds
    And I click good for appettite
    And I select all other Symptoms
    And I input observation and intervention
    And I click projectile for type
    And I click small for amount
    And I click watery and input other for character
    And I input frequency
    And I input observation and intervention1
    #Nutrition/Diet (select all)
    And I select all nutrition or diet
    And I input ml for fluid restriction and other nutrition or diet
    #Enteral Nutrition
    And I click dobhoff for feeding via
    And I input tube insertion date
    And I click pump for formula delivery system
    And I input amount and ml for feeding formula
    And I input amount and ml for liquid supplement
    And I input ml and hours per day for pump rate for hours
    And I click open system for enteral feeding system
    And I input hours if residual volume over for hold feeling for
    And I input ml for hold feeling for
    And I input ml for Gastric residual amount
    And I click yes for Tolerating feedings well and NPO
    And I input NPO
    And I select all for Ostomy care or feedings by
    And I input observation and intervention2
    #Genitourinary Status
    #Urine clarity (select all except clear)
    And I select all urine clarity except clear
    And I click straw and input other for urine color
    And I click yes and input description for urine odor
    And I click yes for abnormal elimination
    And I select all for abnormal elimination
    #Special procedures
    And I click yes for special procedure
    And I select all for special procedure
    And I input observation and intervention3
    #Indwelling catheter
    And I click urethral for catheter type
    And I click 14 for catheter size
    And I click 5 for balloon inflation
    And I click 2-way for catheter lumens
    And I input day for catheter change
    And I input date last change
    And I click done for catheter change
    And I click bedside and leg bag for drainage bag
    And I input days for MD ordered irrigation frequency
    And I click as needed, done and none for MD-ordered irrigation frequency
    And I input amount for MD-ordered irrigation solution
    And I input ml and click none for MD-ordered irrigation solution
    And I input observation and intervention4
    #Intermittent Catheterization
    And I click 16 for catheter size
    And I input frequency for Intermittent Catheterization
    And I select all for done by
    And I input observation and intervention5
    #Nephrostomy
    And I input days and click as needed and done for Nephrostomy dressing change
    And I input days and click as needed and done for Nephrostomy bag change
    And I input days and click as needed and done for MD-ordered irrigation frequency
    And I input amount and ml and click none for MD-ordered Irrigation solution
    And I input observation and intervention6
    And I input days for Urostomy pouch change frequency
    And I input observation and intervention7
    And I click daily for change frequency
    And I input other in change frequency
    And I select all drainage bag
    And I input observation and intervention8
    #Hemodialysis
    And I click AV shunt and input location for AV access
    And I click permacath and input other for AV access
    And I click yes for Bruit present and Thrill strong
    And I click all dialysis schedule
    And I input dialysis center and phone
    And I input observation and intervention9
    #Peritoneal Dialysis
    And I click Continuous Ambulatory Peritoneal Dialysis CAPD for type
    And I input APD machine
    And I select all for dialysate
    And I input dwell time and hours
    And I select all for peritoneal dialysis done by
    And I input observation and intervention10
    And I click 1 for M1600
    #Lower GI Status
    And I input date last BM
    And I select all for bowel movement
    And I click soft and input other for stool character
    And I click yellow or brown and input other for stool color
    And I click effective and click and input MD notified
    And I click as needed and input other for laxative or Enema
    And I input observation and intervention11
    #Lower GI Ostomy
    And I click colostomy for ostomy type
    And I input cm for stoma diameter
    And I click healed for ostomy wound
    And I select all for care done by for Lower GI Ostomy
    And I input observation and intervention12
    And I click 0 for M1620
    And I click save button
    # NUTRITION / ELIMINATION TAB End -------------

Scenario: Automating Entry Form of Neurologic / Behavioral Tab
    Given I Login5
    And I visit patient admitted page
    And I click discharge tab
    When I search the patient
    And I click the added patient
    And I click OASIS - Discharge from Agency
    And I click Edit Button
    And I click Neurologic or Behavioral Tab

    # NEUROLOGIC/BEHAVIORAL TAB Start -------------
    And I click left greater than right for size manual
    And I click left for non-reactive
    #Mental status (select all)
    And I select all for mental status
    And I click adequate MD notified for sleep or rest
    #Hand grips
    And I click left and right for strong
    And I click left and right for weak
    #Other signs (select all)
    And I select all for other signs
    #Weakness
    And I click left and right for upper extremity Weakness
    And I click left and right for lower extremity Weakness
    And I click left for hemiparesis
    #Paralysis
    And I click left for hemiplegia and paraplegia
    #Tremors
    And I click left, right and fine for upper extremity Tremors
    And I click left, right and fine for lower extremity Tremors
    #Seizure
    And I click grand mal
    And I input date of last Seizure
    And I input duration
    And I input observation and intervention
    And I click 1 for M1700
    And I click 1 for M1710
    And I click 2 for M1720
    And I click 2 for M1740
    And I click 2 for M1745
    # Thought Process, Affect and Behavioral Status (select all)
    And I select all for Thought Process, Affect and Behavioral Status
    And I click save button
    # NEUROLOGIC/BEHAVIORAL TAB End -------------


Scenario: Automating Entry Form of ADL / IADL / Musculoskeletal Tab
    Given I Login7
    And I visit patient admitted page
    And I click discharge tab
    When I search the patient
    And I click the added patient
    And I click OASIS - Discharge from Agency
    And I click Edit Button
    And I click ADL or IADL or Musculoskeletal Tab

    # ADL/IADL/Musculoskeletal TAB Start -------------
    #Musculoskeletal Status
    And I click strong for muscle strength
    And I click limited for Range of motion
    # limited (select all)
    And I select all for limited
    And I click independent for Bed mobility
    And I click independent for Transfer ability
    And I click steady for Gait or Ambulation
    And I click good for balance
    And I input seconds for Timed Up & Go
    And I click practiced once before actual test and unable to perform for Timed Up & Go
    And I click low for risk for falls
    And I click left and right for amputation
    And I click BK, AK, UE and input other for amputation
    And I click new, input location and click cast for fracture
    And I input observation and intervention
    #If cast is present, assessment of extremity distal to cast
    And I click pink for color
    And I click strong for Pulses
    And I click < 3 sec for Capillary refill
    And I click warm for temperature
    And I click normal for sensation
    And I click able to move for motor function
    And I click yes for dwelling
    And I input intervention
    # Functional Limitations (select all)\
    And I select all functional Limitations
    # Activities Permitted (select all)
    And I select all activities Permitted
    And I click 2 for M1800 - Grooming
    And I click 1 for M1810 - Current Ability to Dress Upper Body
    And I click 1 for M1820 - Current Ability to Dress Lower Body
    And I click 2 for M1830 - Bathing
    And I click 1 for M1840 - Toilet Transferring
    And I click 1 for M1845 - Toileting Hygiene
    And I click 1 for M1850 - Transferring
    And I click 1 for M1860 - Ambulation or Locomotion
    And I click 1 for M1870 - Feeding or Eating
    #Section GG Functional Abilities and Goals
    #GG0130. Self‐Care
    And I select 6 on A - Eating
    And I select 5 on B - Oral Hygiene
    And I select 5 on C - Toileting Hygiene
    And I select 5 on E - Shower or bathe self
    And I select 5 on F - Upper body dressing
    And I select 5 on G - Lower body dressing
    And I select 5 on H - Putting on or taking off footwear
    #GG0170. Mobility
    And I select 5 on A - Roll left and right for Discharge Performance
    And I select 5 on B - sit to lying for Discharge Performance
    And I select 5 on C - lying to sitting on side of bed for Discharge Performance
    And I select 5 on D - sit to stand for Discharge Performance
    And I select 5 on E - Chair or bed-to-chair transfer for Discharge Performance
    And I select 5 on F - toilet transfer for Discharge Performance
    And I select 5 on G - Car transfer for Discharge Performance
    And I select 6 on I - Walk ten feet for Discharge Performance
    And I select 5 on J - walk fifty feet with two turns for Discharge Performance
    And I select 5 on K - one-fifty feet for Discharge Performance
    And I select 5 on L - walking ten feet on uneven surfaces for Discharge Performance
    And I select 6 on M - one step - curb for Discharge Performance
    And I select 5 on N - four steps for Discharge Performance
    And I select 5 on O - Twelve steps for Discharge Performance
    And I select 5 on P - Picking up object for Discharge Performance
    And I select 1-yes on Q - Does patient use wheelchair and or scooter
    And I select 5 on R - wheel fifty feet with two turns for Discharge Performance
    And I select 2 on RR3 - Indicate the type of wheelchair or scooter used
    And I select 5 on S - wheel one-fifty feet for Discharge Performance
    And I select 2 on SS3 - Indicate the type of wheelchair or scooter used
    And I click save button
    # ADL/IADL/Musculoskeletal TAB End -------------

Scenario: Automating Entry Form of Medication Tab
    Given I Login8
    And I visit patient admitted page
    And I click discharge tab
    When I search the patient
    And I click the added patient
    And I click OASIS - Discharge from Agency
    And I click Edit Button
    And I click Medication Tab

    # MEDICATION TAB Start -------------
    And I click 1 - yes for M2005
    And I click 1 - yes for M2016
    And I click 1 for M2020
    And I click save button
    # MEDICATION TAB End -------------

Scenario: Automating Entry Form of Care Management Tab
    Given I Login9
    And I visit patient admitted page
    And I click discharge tab
    When I search the patient
    And I click the added patient
    And I click OASIS - Discharge from Agency
    And I click Edit Button
    And I click Care Management Tab

    # CARE MANAGEMENT TAB Start -------------
    And I click caregiver always available and reliable for caregiving status
    And I input caregiver name, relationship, phone, name of facility, phone for facility, contact person and community resources utilized
    #(M2102) - Types and Sources of Assistance
    And I click 1 for ADL assistance
    And I click 1 for Medication administration
    And I click 1 for Medical procedures or treatments
    And I click 1 for Supervision and safety
    And I click save button
    # CARE MANAGEMENT TAB End -------------

Scenario: Automating Entry Form of Emergent Care
    Given I Login10
    And I visit patient admitted page
    And I click discharge tab
    When I search the patient
    And I click the added patient
    And I click OASIS - Discharge from Agency
    And I click Edit Button
    And I click Emergent Care Tab

    # Emergent Care TAB Start -------------
    And I click Yes, used hospital emergency department WITHOUT hospital admission for M2301
    And I select all that apply for M2310
    #Intervention Synopsis (clicking yes to all)
    And I click yes to all Intervention Synopsis
    And I click 1 - Hospital for M2410
    And I click 1 - Hospital for M2420
    And I input Discharge-Transfer-Death Date
    #Section J: Health Conditions
    #J1800 - Any Falls Since SOC/ROC
    And I click yes for J1800
    #J1900 - Number of Falls Since SOC/ROC
    And I input 1 for A - No Injury
    And I input 1 for B - Injury
    And I input 1 for C - Major Injury
    And I input additional assessment
    And I click all reasons for discharge
    And I click save button


    # Emergent Care TAB End -------------










 