Feature: Automating Entry Form of CHHA - HHA Visit

Scenario: Automating Entry Form of CHHA - HHA Visit
    Given I Login
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click CHHA - HHA Visit

    # CHHA - HHA Visit Start -------------
    And I click Edit Button
    And I input time in and time out
    #VITAL SIGNS PARAMETERS
    And I input for greater than SBP, DBP, Temp, Pulse-Radial and Resp
    And I input for less than SBP, DBP, Temp, Pulse-Radial and Resp
    #VITAL SIGNS
    And I input blood pressure, temp, pulse-radial, resp, last BM and weight
    #Pain
    And I click denies pain
    And Input pain location
    And I click 1 for present level
    #TASK
    #Personal
    And I click completed for Bed bath, Chair bath, Shower, Tub Bath, Hair shampoo and Hair care or comb hair
    And I click completed for Oral care or comb dentures, Skin or Foot care, Shave or Groom, Nail care, Perineal care and Assist with dressing
    #Nutrition
    And I click completed for Meal preparation and Assist with feeding
    And I click Encourage
    And I click completed for Fluid intake
    #Household Tasks
    And I click completed for Light housekeeping, Change bed linen, Wash clothes and Assist with errands
    #Activities
    And I click completed for Repositioning and turing in bed
    #Assist with mobility/transfer (click all)
    And I click all Assist with mobility or transfer
    And I click completed for Assist with mobility or transfer
    #Assist in ambulation (click all)
    And I click all for Assist in ambulation
    And I click completed for Assist in ambulation
    #Range of motion (click all)
    And I click all for range of motion
    And I click completed for range of motion
    #Assist in prescribed exercises per (click all)
    And I click all for Assist in prescribed exercises per
    And I click completed for Assist in prescribed exercises per and Equipment care
    #Procedures
    And I click completed for Medical reminder, Assist with elimination, Incontinence care, Record bowel movement, Urinary catheter care, Record intake and output, Ostomy care and Empty drainage bag 
    And I click inspect and reinforce
    And I click completed for Wound dressing
    And I input notes
    And I click save button

    # CHHA - HHA Visit End -------------

