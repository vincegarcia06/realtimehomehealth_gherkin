Feature: Wound Management Test Case/Scenarios

Scenario: Adding Patient
    Given I Login
    And I visit admission page and Modal will Display
    When I click skip
    Then Modal will disappear
   
    #Patient Information section
    When I input referral date and hours, click auto assign and input planned SOC date
    # And I input lastname, firstname, middle initial, suffix and birthdate
    And I input in information
        |  firstname  | lastname  | middleinitial | suffix | birthdate |
        |  Kovy | Sumera  | P | Jr. | 12061997 |
    And I check male
    And I select marital status "Single", ethnicity "Asian" and language "English"
    And I input Social Security Number

    #Patient Address section
     And I input in address
        |  streetaddress  | additionaldirections  | majorcrossstreet | city |
        |  1752 Wilson Ave | 1752 Wilson Ave  | Arcadia | Arcadia |
    And Select state

Scenario: W002 - Adding of Wound functionality - Wound 1
    Given I Login 1
    When I Visit Patient List
    And I search the patient "WoundAutomated123"
    And Click the patient
    And I click wound management tab
    And I click OASIS wound reference
    And I click Edit Button
    And I click on human image or pin wound
    And I click yes
    And Select location and wound type
    And Upload wound image
    And I click digital measurement
    And I click edit digital measurement
    Then Modal will display for wound digital instrument
    When I trace the wound image
    And I click save Button
    Then Modal will disapper
    And It will notify wound digital measurement has been set
    And I click Save Button for wound#1
    Then It should display notification "successfully save"

Scenario: W003 - Adding of Wound functionality - Wound 2
    Given I Login 2
    When I Visit Patient List
    And I search the patient "WoundAutomated123"
    And Click the patient
    And I click wound management tab
    And I click OASIS wound reference
    And I click Edit Button
    And I click on human image or pin wound
    And I click yes
    And Select location and wound type
    And Upload wound image
    And I click digital measurement
    And I click edit digital measurement
    Then Modal will display for wound digital instrument
    When I trace the wound image
    And I click save Button
    Then Modal will disapper
    And It will notify wound digital measurement has been set
    And I click Save Button for wound#2
    Then It should display notification "successfully save"


