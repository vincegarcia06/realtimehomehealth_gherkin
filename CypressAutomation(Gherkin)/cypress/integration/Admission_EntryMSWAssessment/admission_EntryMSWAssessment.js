Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });

  const dayjs = require('dayjs')
  
  
  Given('I Login', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Will, Sumera P. Jr.')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(5000)
    });

    And('I click MSW Assessment', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(16) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click MSW - Assessment
        cy.wait(5000)
    });

    And('I input time in and time out', () => {
        cy.get('#time_in').type('1200') //inputting time in
        cy.get('#time_out').type('1900') //inputting time out
    });
    //Evaluation and Plan of Care
    //Plan of Care/Medical Social Work Orders(click all)
    And('I select all for Plan of Care or Medical Social Work Orders', () => {
        cy.get('[rname="orders"]').click({multiple:true})
    });
    
    And('I click crisis intervention to', () => {
        cy.get('[chk="field.orders"] > :nth-child(2) > :nth-child(4) > :nth-child(1) > .radio > .ng-pristine').click() //clicking crisis intervention to
    });
    //Order Crises(click all)
    And('I click all order crisis', () => {
        cy.get('[rname="order_crises"]').click({multiple:true})
    });
    And('I input Home Setting or Environment', () => {
        cy.get('tbody > :nth-child(4) > :nth-child(2) > .global__txtbox').type('Test') //inputting Home Setting / Environment
    });
    And('I input Emergency Contact', () => {
        cy.get(':nth-child(5) > :nth-child(2) > .global__txtbox').type('Test') //inputting Emergency Contact
    });
    And('I input Support System', () => {
        cy.get(':nth-child(6) > :nth-child(2) > .global__txtbox').type('Test') //inputting Support System
    });
    And('I input Homebound Status', () => {
        cy.get(':nth-child(7) > :nth-child(2) > .global__txtbox').type('Test') //inputting Homebound Status
    });
    //Mental Status / Behavior (click all)
    And('I click all Mental Status or Behavior', () => {
        cy.get('[rname="mental_statuses"]').click({multiple:true})
        cy.get(':nth-child(9) > td > .row > .col-sm-6 > :nth-child(4) > .m-0 > .radio > .ng-pristine').click() //clicking other
        cy.get(':nth-child(9) > td > .row > .col-sm-6 > :nth-child(4) > .display-ib.ng-isolate-scope > .global__txtbox').type('Test') //inputting other
    });
    //Financial Status (click all)
    And('I click all Financial Status', () => {
        cy.get('[rname="financial_statuses"]').click({multiple:true})
        cy.get('td > .row > .col-sm-6 > :nth-child(4) > .m-0 > .radio > .ng-pristine').click() //clicking other
        cy.get(':nth-child(11) > td > .row > .col-sm-6 > :nth-child(4) > .display-ib.ng-isolate-scope > .global__txtbox').type('Test') //inputting other
    });
    And('I input Clinical Findings / Problems', () => {
        cy.get(':nth-child(13) > td > .fg-line > .form-control').type('Test') //inputting Clinical Findings / Problems
    });
    And('I input Goals', () => {
        cy.get(':nth-child(15) > td > .fg-line > .form-control').type('Test') //inputting Goals
    });
    And('I input Interventions', () => {
        cy.get(':nth-child(17) > td > .fg-line > .form-control').type('Test') //inputting Interventions
    });
    And('I input Referrals', () => {
        cy.get(':nth-child(19) > td > .fg-line > .form-control').type('Test') //inputting Referrals
    });
    And('I input Outcome & Plan', () => {
        cy.get(':nth-child(21) > td > .fg-line > .form-control').type('Test') //inputting Outcome & Plan
    });
    //Frequency
    And('I click 1 Visit x 60 days', () => {
        cy.get('.m-0.pull-left > :nth-child(1) > .ng-pristine').click() //clicking 1 Visit x 60 days
    });
    And('I click and input other', () => {
        cy.get('.m-l-15 > .m-0 > .radio > .ng-pristine').click() //clicking other
        cy.get('.m-l-15 > .display-ib.ng-isolate-scope > .global__txtbox').type('Test') //inputting other
    });
    //Discharge Summary
    //Reason for Discharge (click all)
    And('I click all Reason for Discharge', () => {
        cy.get('[rname="dc_reasons"]').click({multiple:true})
        cy.get(':nth-child(3) > :nth-child(4) > .m-0 > .radio > .ng-valid').click() //clicking other
        cy.get(':nth-child(3) > :nth-child(4) > div.ng-isolate-scope > .global__txtbox').type('Test')
    });
    //Overall Status of Patient at Discharge
    And('I click all Overall Status of Patient at Discharge', () => {
        cy.get('[rname="dc_mental_statuses"]').click({multiple:true})
        cy.get(':nth-child(4) > .m-0 > .radio > .ng-pristine').click() //clicking other
        cy.get(':nth-child(1) > :nth-child(1) > .row > :nth-child(2) > :nth-child(4) > div.ng-isolate-scope > .global__txtbox').type('Test') //inputting other
    });
    //Patient's Functional Status ADL's
    And('I click independent for Patients Functional Status ADLs', () => {
        cy.get(':nth-child(1) > :nth-child(2) > .radio > .ng-valid').click()
    });
    And('I click independent for Patients Functional Status IADLs', () => {
        cy.get(':nth-child(2) > :nth-child(2) > .radio > .ng-valid').click()
    });

    And('I input Continuing Level of In-Home Care', () => {
        cy.get(':nth-child(30) > td > .fg-line > .form-control').type('Test') //inputting Continuing Level of In-Home Care
    });
    And('I input Problems Identified', () => {
        cy.get(':nth-child(32) > td > .fg-line > .form-control').type('Test') //inputting Problems Identified
    });
    And('I input Status of Problems at Discharge', () => {
        cy.get(':nth-child(34) > td > .fg-line > .form-control').type('Test') //inputting Status of Problems at Discharge
    });
    And('I input Summary of Care Provided', () => {
        cy.get(':nth-child(36) > td > .fg-line > .form-control').type('Test') //inputting Summary of Care Provided
    });
    And('I click goals met', () => {
        cy.get(':nth-child(37) > td > :nth-child(1) > .ng-valid').click() //clicking Goals meT
    });
    And('I click save button', () => {
        cy.get('.btn__success').click() //clicking save button
        cy.wait(10000)
    });



