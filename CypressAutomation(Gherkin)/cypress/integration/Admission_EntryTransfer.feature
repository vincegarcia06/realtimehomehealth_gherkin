Feature: Automating Entry Form of Transfer - not discharged

Scenario: Automating Entry Form of Demographics / Clinical Record
    Given I Login
    And I visit patient admitted page
    And I click in-patient tab
    When I search the patient
    And I click the added patient
    And I click Transfer - not discharged
    And I click Edit Button

    # DEMOGRAPHICS/CLINICAL RECORD TAB Start -------------
    And I input time in and time out
    And I input date assessment completed
    And I selecting 1 Medicare traditional fee for service for Current Payment Sources for Home Care
    And I selecting 1 for Influenza Vaccine Data Collection Period
    And I selecting 1 for Influenza Vaccine Received
    And I click No for Pneumococcal Vaccine
    And I click 3 for Reason Pneumococcal Vaccine not received
    And I click save button
    # DEMOGRAPHICS/CLINICAL RECORD TAB End -------------

Scenario: Automating Entry Form of Medication
    Given I Login1
    And I visit patient admitted page
    And I click in-patient tab
    When I search the patient
    And I click the added patient
    And I click Transfer - not discharged
    And I click Edit Button
    And I click Medication Tab

    # MEDICATION TAB Start -------------
    And I click 1 - yes for M2005
    And I click 1 - yes for M2016
    And I click save button
    # MEDICATION TAB End -------------

Scenario: Automating Entry Form of Emergent Care
    Given I Login2
    And I visit patient admitted page
    And I click in-patient tab
    When I search the patient
    And I click the added patient
    And I click Transfer - not discharged
    And I click Edit Button
    And I click Emergent Care Tab

    # Emergent Care TAB Start -------------
    And I click Yes, used hospital emergency department WITHOUT hospital admission for M2301
    And I select all that apply for M2310
    #Intervention Synopsis (clicking yes to all)
    And I click yes to all Intervention Synopsis
    And I click 1 - Hospital for M2410
    And I input Discharge-Transfer-Death Date
    #Section J: Health Conditions
    #J1800 - Any Falls Since SOC/ROC
    And I click yes for J1800
    #J1900 - Number of Falls Since SOC/ROC
    And I input 1 for A - No Injury
    And I input 1 for B - Injury
    And I input 1 for C - Major Injury
    And I click save button


    # Emergent Care TAB End -------------


