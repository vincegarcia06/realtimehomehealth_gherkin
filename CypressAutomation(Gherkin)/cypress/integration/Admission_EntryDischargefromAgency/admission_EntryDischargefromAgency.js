Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });

  const dayjs = require('dayjs')
  
  
  Given('I Login', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });
    And('I click discharge tab', () => {
        cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(3) > a').click() //clicking Discharged Tab
        cy.wait(5000)
    });
    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jerwin P. Jr')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(5000)
    });

    And('I click OASIS - Discharge from Agency', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(3) > td:nth-child(2)').click();//click Discharge from Agency
        cy.wait(5000)
    });

    And('I click Edit Button', () => {
        cy.get('.btn__warning').click();//click Edit Button
        cy.wait(5000)
    });

    And('I input time in and time out', () => {
        cy.get('#ti').type('1200') //inputting time in
        cy.get('#to').type('1900') //inputting time out
    });
    And('I input date assessment completed', () => {
        cy.get('#M0090_INFO_COMPLETED_DT').type(dayjs().add(1, 'day').format('MM/DD/YYYY')) //inputting date assessment completed
    });
    And('I selecting 1 Medicare traditional fee for service for Current Payment Sources for Home Care', () => {
        cy.get('#M0150_CPAY_NONE > input').click()
    });
    And('I selecting 1 for Influenza Vaccine Data Collection Period', () => {
        cy.get(':nth-child(26) > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(2) > .radio > .ng-valid').click() //clicking 1
    });
    And('I selecting 1 for Influenza Vaccine Received', () => {
        cy.get(':nth-child(28) > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(1) > .radio > .ng-pristine').click() //clicking 1
    });
    And('I click No for Pneumococcal Vaccine', () => {
        cy.get(':nth-child(30) > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(1) > .radio > .ng-valid').click() //clicking NO
    });
    And('I click 3 for Reason Pneumococcal Vaccine not received', () => {
        cy.get(':nth-child(32) > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(3) > .radio > .ng-pristine').click() //clicking 3
    });
    And('I click save button', () => {
        //SAVE BUTTON
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
        cy.wait(5000)    
    });

    Given('I Login1', () => {
        cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });
    And('I click discharge tab', () => {
        cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(3) > a').click() //clicking Discharged Tab
        cy.wait(5000)
    });
    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jerwin P. Jr')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(10000)
    });

    And('I click OASIS - Discharge from Agency', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(3) > td:nth-child(2)').click();//click Discharge from Agency
        cy.wait(5000)
    });

    And('I click Edit Button', () => {
        cy.get('.btn__warning').click();//click Edit Button
        cy.wait(5000)
    });
    And('I click Vital Signs or Sensory Tab', () => {
        cy.get('#oasis-tabs > label:nth-child(2)').click()
        cy.wait(5000)
    });

    And('I input temperature and click oral', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(2) > input').type('98') // inputting temperature
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking oral for temperature
    });

    And('I input pulse or HR and click radial', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > input').type('3') //inputting Pulse/HR
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking radial for Pulse/HR
    });

    And('I input respiration', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > input').type('3') //inputting respiration
    });

    And('I input systolic and diastolic for BP left arm and clicking sitting', () => {
        cy.get('[name="SOOVS0009"]').type('180') //inputting Systolic - BP Left Arm
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(5) > td > table > tbody > tr > td:nth-child(2) > input:nth-child(3)').type('120')  //inputting Diastolic - BP Left Arm
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(5) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking sitting
    });

    And('I input systolic and diastolic for BP right arm and clicking sitting', () => {
        cy.get('[name="SOOVS0012"]').type('180') //inputting Systolic - BP Right Arm
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(2) > input:nth-child(3)').type('120') //inputting Diastolic - BP Right Arm
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr > td.ng-isolate-scope > div > label:nth-child(1) > input').click() //clicking sitting
    });

    And('I input systolic and diastolic for BP left leg and clicking sitting', () => {
        cy.get('[name="SOOVS0015"]').type('180') //inputting Systolic - BP Left Leg
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(2) > input:nth-child(3)').type('120') //inputting Diastolic - BP Left Leg
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking sitting
    });

    And('I input systolic and diastolic for BP right leg and clicking sitting', () => {
        cy.get('[name="SOOVS0018"]').type('180') //inputting Systolic - BP Right Leg
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr > td:nth-child(2) > input:nth-child(3)').type('120') //inputting Diastolic - Right Leg
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking sitting
    });

    And('I input O2 Saturation % on room air', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr > td:nth-child(2) > input').type('2') //inputting O2 Saturation % on room air
    });

    And('I input O2 Saturation % on O2 and input LPM', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(10) > td > table > tbody > tr > td:nth-child(2) > input').type('2') //inputting O2 Saturation % on O2
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(10) > td > table > tbody > tr > td:nth-child(3) > input').type('2') //inputting lPM on o2 Saturation
    });

    And('I input blood sugar and clicking fbs', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(11) > td > table > tbody > tr > td:nth-child(2) > input').type('120') //inputting blood sugar
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(11) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking FBS
    });

    And('I click actual for weight and height', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(12) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking actual
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(13) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking actual
    });

    And('Clicking yes for evidence for infection and input description', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td.p-5.b-r-n.b-l-n > div.m-t-2.p-0.display-ib.ng-isolate-scope > label:nth-child(2) > input').click() //clicking Yes for evidence for infection
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td.p-5.b-r-n.b-l-n > div:nth-child(2) > input').type('test') //inputting description
    });

    And('I click MD and CM or Supervisior', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div > label:nth-child(1) > input').click() //clicking MD in notified
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div > label:nth-child(2) > input').click() //clicking CM/Supervisor
    });

    And('Clicking yes for NewChangeHold medications and input description', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div.m-t-2.p-0.display-ib.ng-isolate-scope > label:nth-child(2) > input').click() //clicking yes for new/change/hold medications
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div:nth-child(2) > input').type('Test') //inputting description
    });

    And('I input observation and intervention', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(16) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(17) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
    });

    And('I input observation and intervention', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(16) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(17) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
    });

    And('Clicking 1 for frequency pain', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click() //clicking 1 for frequency pain
    });

     // Location 1
    And('I input pain location', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(1) > input').type('Test') //inputting pain location
    });
    And('I select type', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > a').click() //clicking dropdown for type
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking type
  });
    And('I select present level', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(3) > div > div > div > a').click() //clicking dropdown for present level
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(3) > div > div > div > div > ul > li:nth-child(5)').click() //clicking personal level
  });
    And('I select worst level', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(4) > div > div > div > a').click() //clicking dropdown worst level
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(5)').click() //clicking worst level
  });
    And('I select acceptable level', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(5) > div > div > div > a').click() //clicking acceptable level dropdown
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(5) > div > div > div > div > ul > li:nth-child(5)').click() //clicking acceptable level
  });
    And('I select level after meds', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(6) > div > div > div > a').click() //clicking level after meds dropdown
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(6) > div > div > div > div > ul > li:nth-child(4)').click() //clicking level after meds
  });

    // Location 2
    And('I input pain location1', () => {
        cy.get(':nth-child(3) > :nth-child(1) > .global__txtbox').type('Test') //inputting pain location
    });
    And('I select type1', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > div > a').click() //clicking dropdown for type
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking type
  });
    And('I select present level1', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(3) > div > div > div > a').click() //clicking dropdown for present level
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(3) > div > div > div > div > ul > li:nth-child(2)').click() //clicking personal level
  });
    And('I select worst level1', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > div > a').click() //clicking dropdown worst level
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(6)').click() //clicking worst level
  });
    And('I select acceptable level1', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(5) > div > div > div > a').click() //clicking acceptable level dropdown
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(5) > div > div > div > div > ul > li:nth-child(6)').click() //clicking acceptable level
  });
    And('I select level after meds1', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(6) > div > div > div > a').click() //clicking level after meds dropdown
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(6) > div > div > div > div > ul > li:nth-child(7)').click() //clicking level after meds
  });

    And('I select all character pain', () => {
        cy.get('[rname="SOOPA0015"]').click({multiple:true})
    });
    And('I select all non verbal signs of pain', () => {
        cy.get('[rname="SOOPA0018"]').click({multiple:true})
    });
    And('I select all what makes the pain better', () => {
        cy.get('[rname="SOOPA0022"]').click({multiple:true})
        cy.get('[style="width: 180px;"] > .p-0 > .global__txtbox').type('Test')
    });
    And('I select all what makes the pain worse', () => {
        cy.get('[rname="SOOPA0020"]').click({multiple:true})
        cy.get('[style="width: 200px;"] > .p-0 > .global__txtbox').type('Test')
    });

    And('I click yes for pain mediaction and input description for profile', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div.display-ib.m-l-15 > label > input').click() // clicking yes for pain medication
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr > td:nth-child(2) > div > input').type('Test') // inputting medication profile
  });
    And('I click daily for how often pain meds needed', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td:nth-child(2) > label > input').click() //clicking how often pain meds needed
    });
    And('I click effective pain medication effectiveness', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(7) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div > label > input').click() //clicking Pain medication effectiveness
    });
    And('I click yes for physician aware of pain', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(8) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div > label > input').click() //clicking yes for Physician aware of pain
    });
    And('I input observation and intervention1', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td.p-5.b-l-n > input').type('Test') //inputting obsevation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td.p-5.b-l-n > input').type('Test') //inputting intervention
  });
    
    //Eyes
    And('I click left and right for cataract', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click() //clicking left for cataract
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > label > input').click() //clicking right for cataract
  });
    And('I click left and right for glaucoma', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //clicking left for glaucoma
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > label > input').click() //clicking right for glaucoma
  });
    And('I click left and right for redness', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click() //clicking left for redness
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(3) > label > input').click() //clicking right for redness
  });
    And('I click left and right for pain', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > label > input').click() //clicking left for pain
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(3) > label > input').click() //clicking right for pain
  });
    And('I click left and right for itching', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > label > input').click() //clicking left for itching
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(3) > label > input').click() //clicking right for itching
  });
    And('I click left and right for ptsosis', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > label > input').click() //clicking left for ptsosis
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(6) > label > input').click() //clicking Right for ptosis
  });
    And('I click left and right for sclera reddened', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(5) > label > input').click() //clicking left for Sclera reddened
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(6) > label > input').click() //clicking Right for Sclera reddened
  });
    And('I click left and right for edema of eyelids', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(5) > label > input').click() //clicking left for Edema of eyelids
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(6) > label > input').click() //clicking Right for Edema of eyelids
  });
    And('I click left and right for blurred vision', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(5) > label > input').click() //clicking left for Blurred vision
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(6) > label > input').click() //clicking Right for Blurred vision
  });
    And('I click left and right for blind', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(5) > label > input').click() //clicking left for Blind	
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(6) > label > input').click() //clicking Right for Blind	
  });
    And('I click left and right for exudate from eyes', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(8) > label > input').click() //clicking left for Exudate from eyes	
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(9) > label > input').click() //clicking Right for Exudate from eyes	
  });
    And('I click left and right for excessive tearing', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(8) > label > input').click() //clicking left for Excessive tearing
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(9) > label > input').click() //clicking Right for Excessive tearing
  });
    And('I click left and right for macular degeneration', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(8) > label > input').click() //clicking left for Macular degeneration
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(9) > label > input').click() //clicking Right for Macular degeneration
  });
    And('I click left and right for retinopathy', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(8) > label > input').click() //clicking left for Retinopathy
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(9) > label > input').click() //clicking Right for Retinopathy
  });
    And('I input other and click left and right for other status', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(7) > input').type('Test') //inputting other status
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(8) > label > input').click() //clicking left for other status
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(9) > label > input').click() //clicking Right for other status
   });

    //Ears
    And('I click left and right for HOH', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click() //clicking left for HOH
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click() //clicking Right for HOH
   });
    And('I click left and right for Hearing aid', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click() //clicking left for Hearing aid
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click() //clicking Right for Hearing aid
   });
    And('I click left and right for Deaf', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click() //clicking left for Deaf
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label > input').click() //clicking Right for Deaf
   });
    And('I click left and right for Tinnitus', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > div > label > input').click() //clicking left for Tinnitus
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(6) > div > label > input').click() //clicking Right for Tinnitus
   });
    And('I click left and right for Drainage', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(5) > div > label > input').click() //clicking left for Drainage
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(6) > div > label > input').click() //clicking Right for Drainage
   });
    And('I click left and right for Pain for ears', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(4) > td:nth-child(5) > div > label > input').click() //clicking left for Pain
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(4) > td:nth-child(6) > div > label > input').click() //clicking Right for Pain
   });
    And('I input other and click vertigo', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(7) > input').type('Test')
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(7) > div > div > label > input').click()
   });

    //Mouth
    And('I click dentures and click upper, lower and partial', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > label > input').click() //clicking dentures
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > span > label:nth-child(1) > input').click() //clicking upper
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > span > label:nth-child(2) > input').click() //clicking lower
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > span > label.checkbox.checkbox-inline.m-r-5.ng-isolate-scope > input').click() //clicking partial
   });
    And('Clicking poor dentition, gingivitis, toothache and loss of taste for Mouth', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking poor dentition for mouth
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click() //clicking gingivitis for mouth
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click() //clicking toothache for mouth
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click() //clicking loss of taste for mouth
   });
    And('I click lesions and input description for lesion', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div.lopt-left > div > label > input').click() //clicking lesions mouth
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div.cont-opt.m-lopt-80.ng-isolate-scope > input').type('Test') //inputting lesions
   });
    And('I click mass or tumor and input description for mass or tumor', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div.lopt-left > div > label > input').click() //clicking mass/tumor for mouth
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div.cont-opt.m-lopt-105.ng-isolate-scope > input').type('Test') //inputting mass/tumor
   });
    And('I click difficulty of chewing or swallowing', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(1) > td.p-b-5.p-l-5 > div > label > input').click() // clicking Difficulty of chewing/swallowing for mouth
    });
    And('I input others for mouth', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td.p-b-5.p-l-5 > div > div.cont-opt.ng-isolate-scope > input').type('Test') //inputting others for mouth
    });

    //NOSE
    And('I select all for nose', () => {
        cy.get('[rname="SOOSENSORY0056"]').click({multiple:true})
    });
    And('I input lesions for nose', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div.cont-opt.m-lopt-80.ng-isolate-scope > input').type('Test') //inputting lesions for nose
    });
    And('I input others for nose', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > div.cont-opt.m-lopt-80.ng-isolate-scope > div > input').type('Test') //inputting others for nose
    });
    
    //THROAT
    And('I select all for throat', () => {
        cy.get('[rname="SOOSENSORY0064"]').click({multiple:true})
    });
    And('I input lesions for throat', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(11) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div.cont-opt.m-lopt-80.ng-isolate-scope > fieldset > input').type('Test') //inputting lesions for throat
    });
    And('I input others for throat', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(11) > td > table > tbody > tr:nth-child(1) > td.p-b-5.p-l-5 > div > div.cont-opt > div > input').type('Test') //inputting other for throat
    });

    //Speech
    And('I select all for Speech', () => {
        cy.get('[rname="SOOSENSORY0068"]').click({multiple:true})
    });
    And('I input others for Speech', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(13) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div.cont-opt > input').type('Test') //inputting other for speech
    });
    
    //Touch
    And('I select all for Touch', () => {
        cy.get('[rname="SENSORY0071"]').click({multiple:true})
    });
    And('I input others for Touch', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(2) > td.p-l-5 > div > div.cont-opt > input').type('Test') //inputting other for touch
    });

    And('I input observation and intervention2', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(16) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(17) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
   });
    And('I click save button', () => {
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() // clicking save button
        cy.wait(5000)
    });
     // VITAL SIGNS/SENSORY TAB End -------------

    Given('I Login2', () => {
        cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    And('I click discharge tab', () => {
        cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(3) > a').click() //clicking Discharged Tab
        cy.wait(5000)
    });
    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jerwin P. Jr')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(10000)
    });

    And('I click OASIS - Discharge from Agency', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(3) > td:nth-child(2)').click();//click Discharge from Agency
        cy.wait(5000)
    });

    And('I click Edit Button', () => {
        cy.get('.btn__warning').click();//click Edit Button
        cy.wait(5000)
    });

    And('I click Integumentary or Endocrine Tab', () => {
        cy.get('#oasis-tabs > label:nth-child(3)').click()
        cy.wait(5000)
    });

    // INTEGUMENTARY/ENDOCRINE TAB Start -------------
    And('I click pink for skin color and input other', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking skin color
        cy.get('#SOOINT0060').type('Test') //inputting other in skin color
    });
    And('I click warm for skin temp', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking skin temp
    });
    And('I click dry for moisture and input other', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > div > label > input').click() //clicking moisture
        cy.get('#SOOINT0074').type('Test') //inputting other in moisture
    });
    And('I click normal for turgor', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking turgor
    });

    And('I click skin intact, lesion and add lesion icon', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(2) > div > label > input').click() //clicking skin intact
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > div > label > input').click() //clicking lesion
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > div > a > i').click() //clicking add lesion
    });
    And('I select one lesion', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(3) > div > div > div').click() //clicking dropdown (select one lesion)
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(3) > div > div > div > div > ul > li:nth-child(2)').click() //selecting result dropdown (select one lesion)
    });
    And('I input location and comment', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(5) > input').type('Test') //inputting location
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(7) > input').type('Test') //inputting comment
    });
    And('I click wound and add wound icon', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(4) > div > label > input').click() //clicking wound
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(4) > div > a > i').click() //clicking add wound
    });
    And('I input wound, location and comment', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr > td:nth-child(3) > div > div.fg-line > input').type('Pressure Ulcer') //inputting result on wound #1
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr > td:nth-child(5) > div > div.fg-line > input').type('Buttock (R)') //inputting result (Location)
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr > td:nth-child(7) > input').type('Test') //inputting comment on wound #1
    });
    And('I input other for skin integrity', () => {
        cy.get('[name="SOOINT0079"]').type('Test') //inputting other in skin integrity
    });
    And('I input observation and intervention', () => {
        cy.get('#SOOINT0078').type('Test') //inputting observation
        cy.get('#SOOINT0080').type('Test') //inputting intervention
    });

    //Braden Scale for Predicting Pressure Sore Risk
    And('I click 1 for sensory perception', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking sensory perception
    });
    And('I click 1 for moisture', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(3) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking moisture
    });
    And('I click 1 for activity', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(4) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking activity
    });
    And('I click 1 for mobility', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking mobility
    });
    And('I click 1 for nutrition', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(6) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking nutrition
    });
    And('I click 2 for friction and shear', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td.p-0 > table > tbody > tr:nth-child(3) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking friction and shear
    });

    //(M1306)
    And('I click yes for M1306', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
    });
    //(M1307)
    And('I click 1 for M1307', () => {
        cy.get('.m-b-5.ng-isolate-scope > .radio > .ng-valid').click()
    });

    //(M1311) - Current Number of Unhealed Pressure Ulcers/Injuries at Each Stage
    And('I input A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1 and F2', () => {
        cy.get('#M1311_NBR_PRSULC_STG2_A1').type('2') //inputting A1
        cy.get('#M1311_NBR_ULC_SOCROC_STG2_A2').type('2') //inputting A2
        cy.get('#M1311_NBR_PRSULC_STG3_B1').type('2') //inputting B1
        cy.get('#M1311_NBR_ULC_SOCROC_STG3_B2').type('2') //inputting B2
        cy.get('#M1311_NBR_PRSULC_STG4_C1').type('2') //inputting C1
        cy.get('#M1311_NBR_ULC_SOCROC_STG4_C2').type('2') //inputting C2
        cy.get('#M1311_NSTG_DRSG_D1').type('2') //inputting D1
        cy.get('#M1311_NSTG_DRSG_SOCROC_D2').type('2') //inputting D2
        cy.get('#M1311_NSTG_CVRG_E1').type('2') //inputting E1
        cy.get('#M1311_NSTG_CVRG_SOCROC_E2').type('2') //inputting E2
        cy.get('#M1311_NSTG_DEEP_TSUE_F1').type('2') //inputting F1
        cy.get('#M1311_NSTG_DEEP_TSUE_SOCROC_F2').type('2') //inputting F2
   });

    //(M1324) - Stage of Most Problematic Unhealed Pressure Ulcer/Injury that is Stageable
    And('I click 2 for M1324', () => {
        cy.get(':nth-child(4) > :nth-child(1) > .molocked > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(2) > .radio > .ng-pristine').click()
    });
    //(M1330) - Does this patient have a Stasis Ulcer?
    And('I click 2 for M1330', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(5) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(3) > label > input').click()
    });
    //(M1334) - Status of Most Problematic Stasis Ulcer that is Observable
    And('I click 2 for M1334', () => {
        cy.get(':nth-child(6) > :nth-child(1) > .molocked > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(2) > .radio > .ng-pristine').click()
    });
    //(M1340) - Does this patient have a Surgical Wound?
    And('I click 1 for M1340', () => {
        cy.get(':nth-child(7) > :nth-child(1) > .molocked > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(2) > .radio > .ng-valid').click()
    });
    //M1342) - Status of Most Problematic Surgical Wound that is Observable
    And('I click 2 for M1342', () => {
        cy.get(':nth-child(8) > :nth-child(1) > .molocked > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(3) > .radio > .ng-pristine').click()
    });
    
    //Endocrine System (Select All)
    And('I select all for endocrine system', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
        cy.get('#SOOENDO0092').type('Test') //inputting other in endocrine system
   });
   And('I input Hgb A1C and date tested', () => {
    cy.get('#SOOENDO0003').type('Test') //inputting Hgb A1C
    cy.get('#SOOENDO0004').type('04/23/2022') //inputting date tested
    });
    And('I click new for onset', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking new for onset
    });
    And('I input onset date', () => {
        cy.get('#SOOENDO0008').type('04/02/2022') //inputting onset date
    });
    And('I select all for diabetes management', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.p-b-5 > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td.p-l-15.p-b-5 > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.p-l-15.-b-5 > div > label > input').click()
    });
    And('I click yes for Signs of hypoglycemia and hyperglycemia', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div > label:nth-child(2) > input').click() //clicking yes for Signs of hypoglycemia?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div > label:nth-child(2) > input').click() //clicking yes for Signs of hyperglycemia?
    });
    And('I input observation and intervention1', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('Test') //inputting for observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td:nth-child(2) > input').type('Test') //inputting for intervention
    });
    And('I click left and right for Thick or ingrown toenail', () => {
        cy.get('[name="SOOENDO0030"]').click() //clicking left for Thick or ingrown toenail
        cy.get('[name="SOOENDO0031"]').click() //clicking right for Thick or ingrown toenail
    });
    And('I click left and right for Calluses or fissures', () => {
        cy.get('[name="SOOENDO0032"]').click() //clicking left for Calluses or fissures
        cy.get('[name="SOOENDO0033"]').click() //clicking right for Calluses or fissures
    });
    And('I click left and right for Interdigital macerations', () => {
        cy.get('[name="SOOENDO0034"]').click() //clicking left for Interdigital macerations
        cy.get('[name="SOOENDO0035"]').click() //clicking right for Interdigital macerations
    });
    And('I click left and right for Signs of fungal infection', () => {
        cy.get('[name="SOOENDO0036"]').click() //clicking left for Signs of fungal infection
        cy.get('[name="SOOENDO0037"]').click() //clicking right for Signs of fungal infection
    });
    And('I click left and right for Absent pedal pulses', () => {
        cy.get('[name="SOOENDO0040"]').click() //clicking left for Absent pedal pulses
        cy.get('[name="SOOENDO0041"]').click() //clicking right for Absent pedal pulses
    });
    And('I click left and right for Hot, red, swollen foot', () => {
        cy.get('[name="SOOENDO0044"]').click() //clicking left for Hot, red, swollen foot
        cy.get('[name="SOOENDO0045"]').click() //clicking right for Hot, red, swollen foot
    });
    And('I click left and right for Foot deformity -hammer or claw toes', () => {
        cy.get('[name="SOOENDO0028"]').click() //clicking left for Foot deformity (hammer/claw toes)
        cy.get('[name="SOOENDO0029"]').click() //clicking right for Foot deformity (hammer/claw toes)
    });
    And('I click left and right for Limited range of motion of joints', () => {
        cy.get('[name="SOOENDO0038"]').click() //clicking left for Limited range of motion of joints
        cy.get('[name="SOOENDO0039"]').click() //clicking right for Limited range of motion of joints
    });
    And('I click left and right for Decreased circulation - cold foot', () => {
        cy.get('[name="SOOENDO0042"]').click() //clicking left for Decreased circulation (cold foot)
        cy.get('[name="SOOENDO0043"]').click() //clicking right for Decreased circulation (cold foot)	
    });
    And('I click left and right for Burning or tingling sensation, numbness', () => {
        cy.get('[name="SOOENDO0046"]').click() //clicking left for Burning/tingling sensation, numbness	
        cy.get('[name="SOOENDO0047"]').click() //clicking right for Burning/tingling sensation, numbness
    });
    And('I click left and right for Loss of sensation to heat or cold', () => {
        cy.get('[name="SOOENDO0048"]').click() //clicking left for Loss of sensation to heat or cold
        cy.get('[name="SOOENDO0049"]').click() //clicking right for Loss of sensation to heat or cold
    });
    And('I input other and click left and right for other assessment', () => {
        cy.get('[name="SOOENDO0103"]').type('Test') //inputting other in foot assessment
        cy.get('[name="SOOENDO0103L"]').click() //clicking left for other foot assessment
        cy.get('[name="SOOENDO0103R"]').click() //clicking right for other foot assessment
    });
    And('I click daily and input other for foot exam frequency', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td.p-5.b-l-n.b-r-n > div > label:nth-child(1) > input').click() //clicking foot exam frequency
        cy.get('#SOOENDO0019').type('Test') //inputting other for foot exam frequency
    });
    And('I select all for regular done by', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label.checkbox.checkbox-inline.m-l-15.ng-isolate-scope > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > label > input').click()
    });    
    And('I click yes for patient and caregiver on Competent with glucometer use including control testing', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking yes for patient on Competent with glucometer use including control testing?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td:nth-child(3) > label:nth-child(1) > input').click() //clicking yes for caregiver on Competent with glucometer use including control testing?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td:nth-child(4) > input').type('Test')
    });
    And('I click yes for patient and caregiver on Competent with insulin preparation and administration', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking yes for patient on Competent with insulin preparation and administration?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(3) > td:nth-child(3) > label:nth-child(1) > input').click() //clicking yes for caregiver on Competent with insulin preparation and administration?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(3) > td:nth-child(4) > input').type('Test')
    });
    And('I click yes for patient and caregiver on Competent after instructions given and performed return demo', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(4) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking yes for patient on Competent after instructions given and performed return demo?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(4) > td:nth-child(3) > label:nth-child(1) > input').click() //clicking yes for caregiver on Competent after instructions given and performed return demo?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(4) > td:nth-child(4) > input').type('Test')

    });
    And('I click yes for patient and caregiver on Level of knowledge of disease process and management', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(5) > td.b-l-n.b-t-n > label:nth-child(1) > input').click() //clicking yes for patient on Level of knowledge of disease process and management
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(5) > td.b-l-n.b-t-n > label:nth-child(4) > input').click() //clicking yes for caregiver on Level of knowledge of disease process and management
    });
    And('I click Before breakfast and input other for testing frequency', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking testing frequency
        cy.get('[name="SOOENDO0016"]').type('Test') //inputting other in testing frequency
    });
    And('I input Brand or Model for Glucometer or CGM', () => {
        cy.get('.cont-opt > .input-drp > .fg-line > .global__txtbox').type('Test')
    });
    And('I click control testing done', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > div > label > input').click() //clicking control testing done
    });
    And('I input level, mg or dl and clicking yes for within range', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(1) > tbody > tr > td > table > tbody > tr > td:nth-child(1) > input').type('1') //inputting level
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(1) > tbody > tr > td > table > tbody > tr > td:nth-child(2) > input').type('120') //inputting mg/dl
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(1) > tbody > tr > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking yes for within range?
    });
    And('I input level, mg or dl and clicking yes for within range1', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(2) > tbody > tr > td > table > tbody > tr > td:nth-child(1) > input').type('1') //inputting level
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(2) > tbody > tr > td > table > tbody > tr > td:nth-child(2) > input').type('120') //inputting mg/dl
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(2) > tbody > tr > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking yes for within range?
    });
    And('I select for reason testing', () => {
        cy.get('#SOOENDO0102_chosen > ul').click() //clicking inputbox/dropdown
        cy.get('#SOOENDO0102_chosen > div > ul > li:nth-child(1)').click() //selecting result
    });
    And('I input action taken', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(6) > td:nth-child(2) > textarea').type('Test') //inputting action taken
    });
    And('I click save button', () => {
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() // clicking save button
        cy.wait(5000)
    });
    And('modal will display', () => {
        cy.get('.swal2-modal').should('be.visible')
    });
    And('I click access later button', () => {
        cy.get('body > div.swal2-container > div.swal2-modal.show-swal2 > button.swal2-cancel.styled').click() //clicking access later in modal
        cy.wait(5000)
    });
     // INTEGUMENTARY/ENDOCRINE TAB End -------------

    Given('I Login3', () => {
        cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    And('I click discharge tab', () => {
        cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(3) > a').click() //clicking Discharged Tab
        cy.wait(5000)
    });
    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jerwin P. Jr')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(10000)
    });

    And('I click OASIS - Discharge from Agency', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(3) > td:nth-child(2)').click();//click Discharge from Agency
        cy.wait(5000)
    });

    And('I click Edit Button', () => {
        cy.get('.btn__warning').click();//click Edit Button
        cy.wait(5000)
    });

    And('I click Cardiopulmonary Tab', () => {
        cy.get('#oasis-tabs > label:nth-child(4)').click()
        cy.wait(5000)
    });

    // CARDIOPULMONARY TAB Start -------------

    And('I click 1 for M1400', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
    });
    And('I click No for No Breath sounds clear, bilateral', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > div.cont-opt.m-lopt-185.p-r-5 > span > label > input').click() //clicking No Breath sounds clear, bilateral
    });
    And('I click left, right, anterior, posterior, upper, middle, lower for diminished', () => {
        cy.get('[name="SOOCARDIO0171"]').click() //clicking left for Diminished
        cy.get('[name="SOOCARDIO0172"]').click() //clicking Right for Diminished
        cy.get('[name="SOOCARDIO0204"]').click() //clicking Anterior for Diminished
        cy.get('[name="SOOCARDIO0205"]').click() //clicking Posterior for Diminished
        cy.get('[name="SOOCARDIO0206"]').click() //clicking Upper for Diminished
        cy.get('[name="SOOCARDIO0207"]').click() //clicking Middle for Diminished
        cy.get('[name="SOOCARDIO0208"]').click() //clicking Lower for Diminished
   });
    And('I click left, right, anterior, posterior, upper, middle, lower for absent', () => {
        cy.get('[name="SOOCARDIO0175"]').click() //clicking left for Absent
        cy.get('[name="SOOCARDIO0176"]').click() //clicking Right for Absent
        cy.get('[name="SOOCARDIO0209"]').click() //clicking Anterior for Absent
        cy.get('[name="SOOCARDIO0210"]').click() //clicking Posterior for Absent
        cy.get('[name="SOOCARDIO0211"]').click() //clicking Upper for Absent
        cy.get('[name="SOOCARDIO0212"]').click() //clicking Middle for Absent
        cy.get('[name="SOOCARDIO0213"]').click() //clicking Lower for Absent
   });
    And('I click left, right, anterior, posterior, upper, middle, lower for rales crackles', () => {
        cy.get('[name="SOOCARDIO0179"]').click() //clicking left for Rales (crackles)
        cy.get('[name="SOOCARDIO0180"]').click() //clicking Right for Rales (crackles)
        cy.get('[name="SOOCARDIO0214"]').click() //clicking Anterior for Rales (crackles)
        cy.get('[name="SOOCARDIO0215"]').click() //clicking Posterior for Rales (crackles)
        cy.get('[name="SOOCARDIO0216"]').click() //clicking Upper for Rales (crackles)
        cy.get('[name="SOOCARDIO0217"]').click() //clicking Middle for Rales (crackles)
        cy.get('[name="SOOCARDIO0218"]').click() //clicking Lower for Rales (crackles)
   });
    And('I click left, right, anterior, posterior, upper, middle, lower for rhonchi', () => {
        cy.get('[name="SOOCARDIO0268"]').click() //clicking left for Rhonchi
        cy.get('[name="SOOCARDIO0269"]').click() //clicking Right for Rhonchi
        cy.get('[name="SOOCARDIO0270"]').click() //clicking Anterior for Rhonchi
        cy.get('[name="SOOCARDIO0271"]').click() //clicking Posterior for Rhonchi
        cy.get('[name="SOOCARDIO0272"]').click() //clicking Upper for Rhonchi
        cy.get('[name="SOOCARDIO0273"]').click() //clicking Middle for Rhonchi
        cy.get('[name="SOOCARDIO0274"]').click() //clicking Lower for Rhonchi
   });
    And('I click left, right, anterior, posterior, upper, middle, lower for wheeze', () => {
        cy.get('[name="SOOCARDIO0183"]').click() //clicking left for Wheeze
        cy.get('[name="SOOCARDIO0184"]').click() //clicking Right for Wheeze
        cy.get('[name="SOOCARDIO0219"]').click() //clicking Anterior for Wheeze
        cy.get('[name="SOOCARDIO0220"]').click() //clicking Posterior for Wheeze
        cy.get('[name="SOOCARDIO0221"]').click() //clicking Upper for Wheeze
        cy.get('[name="SOOCARDIO0222"]').click() //clicking Middle for Wheeze
        cy.get('[name="SOOCARDIO0223"]').click() //clicking Lower for Wheeze
   });
    And('I click left, right, anterior, posterior, upper, middle, lower for stridor', () => {
        cy.get('[name="SOOCARDIO0187"]').click() //clicking left for Stridor
        cy.get('[name="SOOCARDIO0188"]').click() //clicking Right for Stridor
        cy.get('[name="SOOCARDIO0224"]').click() //clicking Anterior for Stridor
        cy.get('[name="SOOCARDIO0225"]').click() //clicking Posterior for Stridor
        cy.get('[name="SOOCARDIO0226"]').click() //clicking Upper for Stridor
        cy.get('[name="SOOCARDIO0227"]').click() //clicking Middle for Stridor
        cy.get('[name="SOOCARDIO0228"]').click() //clicking Lower for Stridor
   });

    And('I click yes for abnormal breathing patterns', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(4) > td > div.cont-opt.m-lopt-185.p-r-5 > span > label > input').click() //clicking yes for Abnormal breathing patterns
    });
    
    And('I select all abnormal breathing patterns', () => {
        cy.get('[rname="SOOCARDIO0140"]').click({multiple:true})
    });
    
    //Cough
    And('I click yes for cough', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(6) > td > div.cont-opt.p-r-5 > span > span > label > input').click() //clicking yes for cough
    });
    And('I click productive for character', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click() //clicking character of cough
    });
    And('I click white for sputum color', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click() //clicking sputum color of cough
    });
    And('I click thin for sputum character', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click() //clicking sputum character of cough
    });
    And('I click small for sputum amount', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click() //clicking sputum amount of cough
    });
    And('I select all special procedure', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(1) > td:nth-child(2) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(1) > td:nth-child(3) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > input').type('Test') //inputting other for special procedure
    });

    And('I input observation and intervention', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > input').type('Test') //inputting intervention
    });

    // Other form of Oxygen therapy
    //Oxygen Risk Assessment
    And('I click yes for oxygen risk assessment', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr.ng-isolate-scope > td:nth-child(2) > label > input').click() //click yes for Is the patient using oxygen equipment?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //click yes for Does anyone in the home smoke?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click() //click yes for Are oxygen signs posted in the appropriate places?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(2) > label > input').click() //click yes for Are oxygen tanks/concentrators stored safely?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(6) > td.text-center.b-l-n.b-r-n.ng-isolate-scope > label > input').click() //click yes for Is backup O2 tank available?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr.ng-isolate-scope > td.text-center.b-l-n.b-r-n.ng-isolate-scope > label > input').click() //click yes for Does patient/PCG know how to use backup O2?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(5) > label > input').click() //click yes for Are all cords near or related to oxygen intact, secure & properly used?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(5) > label > input').click() //click yes for Is patient educated re: substances that can cause O2 to be flammable?
    });

    //Are there potential sources of open flames identified? (select all)
    And('I click all potential sources of open flames', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label.checkbox.checkbox-inline.m-l-5.ng-isolate-scope > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(3) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(4) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(5) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(6) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(5) > label > input').click() //click yes for Are there potential sources of open flames identified?

    });
    And('I input observation and intervention1', () => {
        cy.get(':nth-child(7) > [colspan="6"] > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting observation
        cy.get(':nth-child(8) > [colspan="6"] > .inner__table-nb > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting intervention
    });

    //Oxygen Theraphy
    And('I click continuous for type', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click() //clicking type

    });
    
    //Oxygen delivery (select all)
    And('I select all oxygen delivery', () => {
        cy.get('[rname="SOOCARDIO0275"]').click({multiple:true})
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(3) > td:nth-child(2) > input').type('Test') //inputting other oxygen delivery
    });

    And('I input liters or minute', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(4) > td.p-b-5.b-l-n > table > tbody > tr > td > div > input').type('10') //inputting liters/minute
    });
    
    //Oxygen source (select all)
    And('I selecy all oxygen source', () => {
        cy.get('[rname="SOOCARDIO0261"]').click({multiple:true})
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(5) > td.p-b-5.b-l-n > table > tbody > tr > td:nth-child(3) > div > input').type('Test') //inputting other for oxygen source
    });

    And('I click yes for backup O2 tank', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(6) > td.p-b-5.b-l-n > table > tbody > tr > td:nth-child(1) > label:nth-child(1) > input').click() //clicking yes for backup O2 tank
    });

    And('I click vendor notified', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(6) > td.p-b-5.b-l-n > table > tbody > tr > td:nth-child(2) > label > input').click() //clicking vendor notified

    });
    And('I input observation and intervention2', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(7) > td:nth-child(2) > label > input').click() //clicking for observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(8) > td:nth-child(2) > label > input').click() //clicking for intervention
    });

    //Other form for Tracheostomy
    And('I inpput brand, trach tube change, date last changed and inner cannula', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td.b-t-n.b-b-n.ng-isolate-scope > span > input').type('Test') //inputting brand/model
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.ng-isolate-scope > div > input').type('4')
        cy.get('#SOOCARDIO0089').type('12042021') //inputting date last change (mm/dd/yyyy)
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.ng-isolate-scope > span > span > input').type('Test') //inputting inner cannula
    });
    And('I input observation and intervention3', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td:nth-child(2) > input').type('Test') //inputting intervention

    });

    //Other form for BiPAP/CPAP
    And('I input brand for BiPAP or CPAP', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(2) > td.ng-isolate-scope > div > input').type('Test') //inputting device brand/model
    });
    And('I click yes for Device working properly and Compliant with use of device', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td.ng-isolate-scope > label.radio.radio-inline.m-r-10.ng-isolate-scope > input').click() //clicking yes for Device working properly?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(4) > td.ng-isolate-scope > label.radio.radio-inline.m-r-10.ng-isolate-scope > input').click() //clicking yes for Compliant with use of device?\
    });
    And('I input observation and intervention4', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td.ng-isolate-scope > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(6) > td.ng-isolate-scope > input').type('Test') //inputting intervention
    });

    //Other form for Suctioning
    And('I click oral', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > label:nth-child(1) > input').click() //clicking oral
    });
    And('I click yes for Is the person performing the suctioning proficient', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(3) > td > label:nth-child(1) > input').click() //clicking yes for Is the person performing the suctioning proficient? 
    });
    And('I click yes for Is the suction equipment setup always ready for use', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(4) > td > label:nth-child(2) > input').click() //clicking yes for Is the suction equipment setup always ready for use?
    });
    
    And('I select all for suction procedure done by', () => {
        cy.get('[rname="SOOCARDIO0336"]').click({multiple:true})
    });
    And('I input observation and intervention5', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
    });

    //Other form for Ventilator
    And('I input brand, ridal volume, FiO2 and assist control for ventilator', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td.b-b-n.b-l-n.ng-isolate-scope > div > input').type('Test') //inputting Brand/Model
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td.b-b-n.b-l-n.ng-isolate-scope > div > input').type('Test') //inputting Tidal volume
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td:nth-child(2) > div > input').type('Test') //inputting FiO2
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(5) > td:nth-child(2) > div > input').type('Test') //inputting Assist control

    });
    And('I input PEEP, SIMV, pressure control and PRVC for ventilator', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td.b-t-n.b-l-n.ng-isolate-scope > div > input').type('Test') //inputting PEEP
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td.b-t-n.b-l-n.ng-isolate-scope > div > input').type('Test') //inputting SIMV	
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td:nth-child(4) > div > input').type('Test') //inputting Pressure control
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(5) > td:nth-child(4) > div > input').type('Test') //inputting PRVC

    });
    And('I input observation and intervention6', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(7) > td:nth-child(2) > input').type('Test') //inputting intervention
    });

    //Other form for PleurX
    And('I input date catheter inserted', () => {
        cy.get('#SOOCARDIO0292').type('12042021') //inputting date catheter inserted (mm/dd/yyyy)
    });
    And('I click daily and input other for drainage frequency', () => {
        cy.get('#SOOCARDIO0293').click() //clicking daily for drainage frequency
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td.b-l-n.ng-isolate-scope > input').type('Test') //inputting other for drainage frequency
    });
    And('I input ml for amount drained', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(4) > td.b-l-n.ng-isolate-scope > input').type('2') //inputting ml for amount drained
    });
    And('I click done', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(4) > td.b-l-n.ng-isolate-scope > div > label > input').click() //clicking done
    });
    
    And('I select all for procedure done by', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(5) > td.b-l-n.ng-isolate-scope > label:nth-child(1) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(5) > td.b-l-n.ng-isolate-scope > label:nth-child(2) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(5) > td.b-l-n.ng-isolate-scope > label:nth-child(3) > input').click()
    });
    And('I input observation and intervention7', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td.ng-isolate-scope > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(7) > td.ng-isolate-scope > input').type('Test') //inputting intervention
    });

    //Cardiovascular
    And('I click regular for heart rhythm', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td:nth-child(2) > div:nth-child(1) > label > input').click() //clicking regular for Heart Rhythm
    });
    And('I click least than three for capillary refill', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr > td.p-r-5 > label > input').click() //clicking < 3 sec for Capillary refill
    });
    And('I click yes for JVD, peripheral edema, chest pain and cardiac device', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td.ng-isolate-scope > label > input').click() //clicking yes for JVD	
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr > td.ng-isolate-scope > label > input').click() //clicking yes for Peripheral edema
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td.ng-isolate-scope > label > input').click() //clicking yes for Chest Pain
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td:nth-child(2) > table > tbody > tr > td:nth-child(2) > label > input').click() //clicking yes for Cardiac device
    });
    And('I select cardiac device', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td:nth-child(2) > table > tbody > tr > td.p-0 > div > div > div').click() //clicking dropdown for cardiac device result
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td:nth-child(2) > table > tbody > tr > td.p-0 > div > div > div > div > ul > li:nth-child(1)').click() //clicking result on cardiac device
    });
    And('I click yes and input other fow weight gain', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td:nth-child(2) > label.radio.radio-inline.m-l-20.ng-isolate-scope > input').click() //clicking yes for weight gain
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td:nth-child(2) > input').type('Test') //inputting other for weight gain
    });

    //Pulses
    And('I click pedal left and right for bounding', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(2) > label > input').click() //clicking pedal-left for Bounding
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(3) > label > input').click() //clicking pedal-right for Bounding
    });
    And('I click popliteal left and right for bounding', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(4) > label > input').click() //clicking Popliteal-left for Bounding
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(5) > label > input').click() //clicking Popliteal-right for Bounding
    });
    And('I click femoral left and right for bounding', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(6) > label > input').click() //clicking Femoral-left for Bounding
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(7) > label > input').click() //clicking Femoral-right for Bounding
    });
    And('I click brachial left and right for bounding', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(8) > label > input').click() //clicking Brachial-left for Bounding
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(9) > label > input').click() //clicking Brachial-right for Bounding
    });
    And('I click radial left and right for bounding', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(10) > label > input').click() //clicking Radial-left for Bounding
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(11) > label > input').click() //clicking Radial-right for Bounding
    });
    And('I input observation and intervention8', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(15) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(16) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
    });

    //Peripheral edema
    And('Clicking +1 for Pedal edema - Left', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //clicking +1 for Pedal edema - Left
    }); 
    And('Clicking +1 for Pedal edema - Right', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click() //clicking +1 for Pedal edema - Right	
    }); 
    And('Clicking +1 for Ankle edema - Left', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(5) > td:nth-child(2) > label > input').click() //clicking +1 for Ankle edema - Left	
    }); 
    And('Clicking +1 for Ankle edema - Right', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(6) > td:nth-child(2) > label > input').click() //clicking +1 for Ankle edema - Right	
    }); 
    And('Clicking +1 for Leg edema - Left', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(7) > td:nth-child(2) > label > input').click() //clicking +1 for Leg edema - Left
    });
    And('Clicking +1 for Leg edema - Right', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(8) > td:nth-child(2) > label > input').click() //clicking +1 for Leg edema - Right
    }); 
    And('Clicking +1 for Sacral edema', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(9) > td:nth-child(2) > label > input').click() //clicking +1 for Sacral edema	
    }); 
    And('Clicking +1 for Generalized edema', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(10) > td:nth-child(2) > label > input').click() //clicking +1 for Generalized edema
    });
    And('I input observation and intervention9', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(11) > td.ng-isolate-scope > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(12) > td.ng-isolate-scope > input').type('Test') //inputting intervention
    });

    //Chest Pain
    //Character (select all)
    And('I select all character', () => {
        cy.get('[rname="SOOCARDIO0141"]').click({multiple:true})
    });
    And('I click left for radiating to shoulder, jaw, neck, arm', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td.p-r-5.p-b-5.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for radiating to shoulder
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.p-r-5.p-b-5.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for radiating to jaw
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td.p-r-5.p-b-5.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for radiating to neck
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(4) > td.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for radiating to arm
    });
    And('I select all for accompanied by', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td.p-r-5.ng-isolate-scope > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(2) > label > input').click()
    });
    And('I input frequency and duration of pain', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(5) > td.ng-isolate-scope > input').type('Test') //inputting frequency of pain
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(6) > td.ng-isolate-scope > input').type('Test') //inputting duration of pain
    });
    And('I input observation and intervention10', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(7) > td.p-b-5.ng-isolate-scope > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(8) > td.p-b-5.ng-isolate-scope > input').type('Test') //inputting intervention
    });

    //Pacemaker
    And('I input brand, rate setting, date implanted and date last tested', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > input').type('Test') //inputting brand/model
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > input').type('20') //inputting Rate setting
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(4) > input').type('12/24/2021') //inputting Date implanted
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(4) > input').type('12/03/2021') //inputting Date last tested
    });
    And('I click no for', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //clicking no
    });
    And('I input observation and intervention11', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > input').type('Test') //inputting intervention
    });
    And('I click save button', () => {
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() // clicking save button
        cy.wait(5000)
    });
     // CARDIOPULMONARY TAB End -------------

    Given('I Login4', () => {
        cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    And('I click discharge tab', () => {
        cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(3) > a').click() //clicking Discharged Tab
        cy.wait(5000)
    });
    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jerwin P. Jr')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(10000)
    });

    And('I click OASIS - Discharge from Agency', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(3) > td:nth-child(2)').click();//click Discharge from Agency
        cy.wait(5000)
    });

    And('I click Edit Button', () => {
        cy.get('.btn__warning').click();//click Edit Button
        cy.wait(5000)
    });

    And('I click Nutrition or Elimination Tab', () => {
        cy.get('#oasis-tabs > label:nth-child(5)').click()
        cy.wait(5000)
    });

    // NUTRITION / ELIMINATION TAB Start -------------
    And('I click soft and input inches in girt for abdomen', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(1) > td.b-r-n.b-l-n.b-b-n > label > input').click() //click soft for abdomen
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(4) > input').type('2') //inputting inches in girt
    });
    And('I click active for bowel sounds', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(1) > td.b-r-n.b-l-n.b-t-n.b-b-n > label > input').click() //clicking active for bowel sounds
    });
    And('I click good for appettite', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(1) > td.b-r-n.b-l-n.b-t-n.b-b-n > label > input').click() //clicking good for appetite
    });
    
    And('I select all other Symptoms', () => {
        cy.get('[rname="SOONUTRI0074"]').click({multiple:true})
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(1) > td:nth-child(4) > input').type('Test') //inputting other symptom
    });
    And('I input observation and intervention', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(4) > td.b-l-n.b-t-n.observation_intervention > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.b-l-n.b-t-n.observation_intervention > input').type('Test') //inputting intervention

    });
    And('I click projectile for type', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(2) > td.b-l-n.b-t-n > label:nth-child(1) > input').click() //clicking projectile for type
    });
    And('I click small for amount', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td.b-l-n.b-t-n > label:nth-child(1) > input').click() //clicking small for amount
    });
    And('I click watery and input other for character', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(4) > td.b-t-n.b-l-n > label:nth-child(1) > input').click() //clicking watery for character\
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(4) > td.b-t-n.b-l-n > span > input').type('Test') //inputting other character
    });
    And('I input frequency', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td.b-t-n.b-l-n > div > input').type('Test') //inputting frequency
    });
    And('I input observation and intervention1', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(6) > td.b-t-n.b-l-n.observation_intervention > div > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(7) > td.b-t-n.b-l-n.observation_intervention > div > input').type('Test') //inputting intervention
    });

    //Nutrition/Diet (select all)
    And('I select all nutrition or diet', () => {
        cy.get('[rname="SOONUTRI0003"]').click({multiple:true})
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(4) > div.cont-opt.m-lopt-45 > input').type('Test') //inputting other nutrition/diet
    });
    And('I input ml for fluid restriction and other nutrition or diet', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > div.m-l-120 > div > div > input').type('6') //inputting ml for fluid restriction
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(4) > div.cont-opt.m-lopt-45 > input').type('Test') //inputting other nutrition/diet
    });
    //Enteral Nutrition
    And('I click dobhoff for feeding via', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(3) > input').click() //clicking dobhoff for feeding via
    });
    And('I input tube insertion date', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > input').type('12/06/2021') //inputting tube insertion date (mm/dd/yyyy)
    });
    And('I click pump for formula delivery system', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking pump for formula delivery system
    });
    And('I input amount and ml for feeding formula', () => {
        cy.get('[name="SOONUTRI0043"]').type('4') //inputting amout for feeding formula
        cy.get('[name="SOONUTRI0044"]').type('4') //inputting ml for feeding formula
   });
    And('I input amount and ml for liquid supplement', () => {
        cy.get('[name="SOONUTRI0045"]').type('4') //inputting amout for liquid supplemet
        cy.get('[name="SOONUTRI0046"]').type('4') //inputting ml for liquid supplemet
   });
    And('I input ml and hours per day for pump rate for hours', () => {
        cy.get('[name="SOONUTRI0053"]').type('4') //inputting ml for Pump rate per hour
        cy.get('[name="SOONUTRI0054"]').type('24') //inputting hours per day for Pump rate per hour
   });
    And('I click open system for enteral feeding system', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking open system for enteral feeding system
    });
    And('I input hours if residual volume over for hold feeling for', () => {
        cy.get('[name="SOONUTRI0077"]').type('4') //inputting hours if residual volume over for hold feeling for
    });
    And('I input ml for hold feeling for', () => {
        cy.get('[name="SOONUTRI0078"]').type('4') //inputting ml for hold feeling for
    });
    And('I input ml for Gastric residual amount', () => {
        cy.get('[name="SOONUTRI0049"]').type('4') //inputting ml for Gastric residual amount
    });
    And('I click yes for Tolerating feedings well and NPO', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td:nth-child(2) > label.radio.radio-inline.m-r-10.ng-isolate-scope > input').click() //clicking yes for Tolerating feedings well?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(10) > td:nth-child(2) > label.radio.radio-inline.m-r-10.ng-isolate-scope > input').click() //clicking yes for NPO?
   });
    And('I input NPO', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(10) > td:nth-child(2) > input').type('Test') //inputting NPO
    });
    And('I select all for Ostomy care or feedings by', () => {
        cy.get('[rname="SOONUTRI0100"]').click({multiple:true})
    });
    And('I input observation and intervention2', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(12) > td:nth-child(2) > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(13) > td:nth-child(2) > input').type('Test') //inputting intervention
   });
    
    //Genitourinary Status
    //Urine clarity (select all except clear)
    And('I select all urine clarity except clear', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(2) > td.p-b-5.ng-isolate-scope > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(1) > td.b-r-n.b-l-n.p-b-5 > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(2) > td.b-r-n.b-l-n.p-b-5 > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(1) > td.b-l-n.p-b-5.ng-isolate-scope > input').type('Test') //inputting other for urine clarity
   });
    And('I click straw and input other for urine color', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td.b-l-n > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click() //clicking straw for urine color
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td.b-l-n > table > tbody > tr:nth-child(2) > td:nth-child(3) > input').type('Test') //inputting other urine color
   });
    And('I click yes and input description for urine odor', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(4) > td.b-l-n > table > tbody > tr > td.b-r-n.b-l-n.ng-isolate-scope > label > input').click() //clicking yes for urine odor
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(4) > td.b-l-n > table > tbody > tr > td:nth-child(3) > input').type('Test') //inputting description
   });
    And('I click yes for abnormal elimination', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(5) > td.b-l-n.subheadv3.ng-isolate-scope > label:nth-child(2) > input').click() //clicking yes Abnormal elimination
    });
    And('I select all for abnormal elimination', () => {
        cy.get('[rname="SOOELIMINATION0139"]').click({multiple:true})
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div.m-l-50 > div > input').type('Test') //inputting other for abnormal elimination
    });

    //Special procedures
    And('I click yes for special procedure', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(7) > td.b-l-n.subheadv3.ng-isolate-scope > label:nth-child(2) > input').click() //clicking yes for Special procedures
    });
    And('I select all for special procedure', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div:nth-child(2) > div > input').type('Test') //inputting other for special procedure
   });
    And('I input observation and intervention3', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td.p-5.b-l-n > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td.p-5.b-l-n > input').type('Test') //inputting intervention
  });

    //Indwelling catheter
    And('I click urethral for catheter type', () => {
        cy.get('#SOOELIMINATION0017').click() //clicking urethral for catheter type
    });
    And('I click 14 for catheter size', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr.catheter__row > td:nth-child(2) > label:nth-child(2) > input').click() //clicking 14 for catheter size
    });
    And('I click 5 for balloon inflation', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr.balloon_inflation__row > td:nth-child(2) > label:nth-child(2) > input').click() //clicking 5 for balloon inflation
    });
    And('I click 2-way for catheter lumens', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking 2-way for catheter lumens
    });
    And('I input day for catheter change', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('3') //inputting days for catheter change
    });
    And('I input date last change', () => {
        cy.get('#sooelimination0156').type('05232021') //date last changed for catheter change
    });
    And('I click done for catheter change', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td:nth-child(2) > div.display-ib.checkbox.m-0.m-t-5.m-l-15 > label > input').click() //clicking done for catherter change
    });
    And('I click bedside and leg bag for drainage bag', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking bedside bag for drainage bag
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td:nth-child(2) > label:nth-child(2) > input').click() //clicking leg bag for drainage bag
   });
    And('I input days for MD ordered irrigation frequency', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td:nth-child(2) > input').type('3') //inputting days for MD-ordered irrigation frequency
    });
    And('I click as needed, done and none for MD-ordered irrigation frequency', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td:nth-child(2) > label:nth-child(4) > input').click() //clicking as needed for MD-ordered irrigation frequency
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td:nth-child(2) > div > label:nth-child(1) > input').click() //clicking done for MD-ordered irrigation frequency\
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td:nth-child(2) > div > label.p-l-18.m-l-5.ng-isolate-scope > input').click() //clicking none for MD-ordered irrigation frequency
   });
    And('I input amount for MD-ordered irrigation solution', () => {
        cy.get('[name="SOOELIMINATION0055"]').type('24') //inputting amount for MD-ordered irrigation solution
    });
    And('I input ml and click none for MD-ordered irrigation solution', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(9) > td:nth-child(2) > input:nth-child(2)').type('4') //inputting ml for MD-ordered irrigation solution
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(9) > td:nth-child(2) > div > label > input').click() //clicking none for MD-ordered irrigation solution
   });
    And('I input observation and intervention4', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(10) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
   });

    //Intermittent Catheterization
    And('I click 16 for catheter size', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(2) > input').click() //clicking 16 for catheter size
    });
    And('I input frequency for Intermittent Catheterization', () => {
        cy.get('[name="SOOELIMINATION0153"]').type('3') //inputting for frequency
    });
    And('I click day and input other for frequency', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(3) > input').click() //clicking day for frequency
        cy.get('[name="SOOELIMINATION0183"]').type('Test') //inputting other for frequency
    });
    And('I select all for done by', () => {
        cy.get('[name="SOOELIMINATION0207"]').click()
        cy.get('[name="SOOELIMINATION0208"]').click()
        cy.get('[name="SOOELIMINATION0209"]').click()
    });
    And('I input observation and intervention5', () => {
        cy.get('[name="SOOELIMINATION0187"]').type('Test') //inputting observation
        cy.get('[name="SOOELIMINATION0210"]').type('Test') //inputting intervention
    });

    //Nephrostomy
    And('I input days and click as needed and done for Nephrostomy dressing change', () => {
        cy.get('[name="SOOELIMINATION0174"]').type('3') //inputting days for Nephrostomy dressing change
        cy.get('[rname="SOOELIMINATION0197"]').click() //clicking as needed for Nephrostomy dressing change
        cy.get('[rname="SOOELIMINATION0214"]').click() //clicking done for Nephrostomy dressing change
    });
    And('I input days and click as needed and done for Nephrostomy bag change', () => {
        cy.get('[name="SOOELIMINATION0177"]').type('3') //inputting days for Nephrostomy bag change
        cy.get('[rname="SOOELIMINATION0198"]').click() //clicking as needed for Nephrostomy bag change
        cy.get('[rname="SOOELIMINATION0215"]').click() //clicking done for Nephrostomy bag change
    });
    And('I input days and click as needed and done for MD-ordered irrigation frequency', () => {
        cy.get('[name="SOOELIMINATION0175"]').type('3') //inputting days for MD-ordered irrigation frequency
        cy.get('[rname="SOOELIMINATION0176"]').click() //clicking as needed for MD-ordered irrigation frequency
        cy.get('[rname="SOOELIMINATION0216"]').click() //clicking done for MD-ordered irrigation frequency
        cy.get('[rname="SOOELIMINATION0223"]').click() //clicking none for  MD-ordered irrigation frequency
    });
    And('I input amount and ml and click none for MD-ordered Irrigation solution', () => {
        cy.get('[name="SOOELIMINATION0178"]').type('3') //inputting amount for MD-ordered Irrigation solution
        cy.get('[name="SOOELIMINATION0179"]').type('3') //inputting ml for MD-ordered Irrigation solution
        cy.get('[rname="SOOELIMINATION0224"]').click() //clicking none for MD-ordered Irrigation solution
    });
    And('I input observation and intervention6', () => {
        cy.get('[name="SOOELIMINATION0185"]').type('Test') //inputting observation
        cy.get('[name="SOOELIMINATION0200"]').type('Test') //inputting intervention
    });

    And('I input days for Urostomy pouch change frequency', () => {
        cy.get('[name="SOOELIMINATION0181"]').type('3') //inputting days for Urostomy pouch change frequency:
    });
    And('I input observation and intervention7', () => {
        cy.get('[name="SOOELIMINATION0186"]').type('Test') //inputting observation
        cy.get('[name="SOOELIMINATION0201"]').type('Test') //inputting intervention
});

    And('I click daily for change frequency', () => {
        cy.get('[rname="SOOELIMINATION0150"]').click() //clicking daily for Change frequency
    });
    And('I input other in change frequency', () => {
        cy.get('[name="SOOELIMINATION0151"]').type('Test') //inputting other in Change frequency
    })
    And('I select all drainage bag', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(3) > td:nth-child(2) > label.checkbox.checkbox-inline.m-r-10.ng-isolate-scope > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(2) > input').click()
   });
    And('I input observation and intervention8', () => {
        cy.get('[name="SOOELIMINATION0188"]').type('Test') //inputting observation
        cy.get('[name="SOOELIMINATION0202"]').type('Test') //inputting intervention
    });

    //Hemodialysis
    And('I click AV shunt and input location for AV access', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking av shunt for AV access
        cy.get('[name="SOOELIMINATION0032"]').type('Test') //inputting location for AV access
   });
    And('I click permacath and input other for AV access', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(2) > td:nth-child(2) > label.checkbox.checkbox-inline.m-l-20.ng-isolate-scope > input').click() //clicking Permacath for AV access
        cy.get('[name="SOOELIMINATION0180"]').type('Test') //inputting other in AV access
   });
    And('I click yes for Bruit present and Thrill strong', () => {
        cy.get('#SOOELIMINATION0034').click() //clicking yes for Bruit present?
        cy.get('#SOOELIMINATION0033').click() //clicking yes for Thrill strong?
   });
    
    And('I click all dialysis schedule', () => {
        cy.get('[rname="SOOELIMINATION0008"]').click({multiple:true})
    });
    And('I input dialysis center and phone', () => {
        cy.get('[name="SOOELIMINATION0036"]').type('Test') //inputting dialysis center
        cy.get('[name="SOOELIMINATION0037"]').type('09123456789') //inputting phone
    });
    And('I input observation and intervention9', () => {
        cy.get('[name="SOOELIMINATION0190"]').type('Test') //inputting observation
        cy.get('[name="SOOELIMINATION0203"]').type('Test') //inputting intervention
    });

    //Peritoneal Dialysis
    And('I click Continuous Ambulatory Peritoneal Dialysis CAPD for type', () => {
        cy.get('#SOOELIMINATION0096').click() //clicking type
    });
    And('I input APD machine', () => {
        cy.get('[name="SOOELIMINATION0094"]').type('Test') //inputting APD machine
    });
    
    And('I select all for dialysate', () => {
        cy.get('[rname="SOOELIMINATION0097"]').click()
        cy.get('[rname="SOOELIMINATION0098"]').click()
        cy.get('[rname="SOOELIMINATION0099"]').click()
        cy.get('[rname="SOOELIMINATION0100"]').click()

    });
    And('I input dwell time and hours', () => {
        cy.get('[name="SOOELIMINATION0066"]').type('12:00') //inputting dwell time
        cy.get('[name="SOOELIMINATION0093"]').type('4') //inputting hours
    });
    And('I select all for peritoneal dialysis done by', () => {
        cy.get('[name="SOOELIMINATION0158"]').click()
        cy.get('[name="SOOELIMINATION0159"]').click()
        cy.get('[name="SOOELIMINATION0160"]').click()
    });
    And('I input observation and intervention10', () => {
        cy.get('[name="SOOELIMINATION0193"]').type('Test') //inputting observation
        cy.get('[name="SOOELIMINATION0204"]').type('Test') //inputting intervention
    });
    And('I click 1 for M1600', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset:nth-child(17) > table > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(2) > label > input').click()
    });
    //Lower GI Status
    And('I input date last BM', () => {
        cy.get('#SOOELIMINATION0171').type('12/04/2021') //inputting date last BM (mm/dd/yyyy)
    });
    
    And('I select all for bowel movement', () => {
        cy.get('[name="SOOELIMINATION0161"]').click()
        cy.get('[name="SOOELIMINATION0083"]').click()
        cy.get('[name="SOOELIMINATION0163"]').click()
        cy.get('[name="SOOELIMINATION0164"]').click()
        cy.get('[name="SOOELIMINATION0043"]').type('5')
        cy.get('[name="SOOELIMINATION0157"]').click()
   });
    And('I click soft and input other for stool character', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(18) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click() //clicking soft for stool character
        cy.get('[name="SOOELIMINATION0104_OTHER"]').type('Test') //inputting other for stool character
  });
    And('I click yellow or brown and input other for stool color', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(18) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click() //clicking yellow/brown for stool color
        cy.get('[name="SOOELIMINATION0088"]').type('Test') //inputting other for stool character
  });
    And('I click effective and click and input MD notified', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(18) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.b-r-n.b-l-n.ng-isolate-scope > label > input').click() //clicking effective for
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(18) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click() //clicking MD notified for
        cy.get('[name="SOOELIMINATION0222"]').type('Test') //inputting MD notified
  });
    And('I click as needed and input other for laxative or Enema', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(18) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.b-r-n.b-l-n.ng-isolate-scope > label > input').click() //clicking as needed for Laxative/Enema
        cy.get('[name="SOOELIMINATION0046_OTHER"]').type('Test') //inputting other for Laxative/Enema
  });
    And('I input observation and intervention11', () => {
        cy.get('[name="SOOELIMINATION0196"]').type('Test') //inputting observation
        cy.get('[name="SOOELIMINATION0205"]').type('Test') //inputting intervention
  });

    //Lower GI Ostomy
    And('I click colostomy for ostomy type', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(2) > td:nth-child(2) > label.radio.radio-inline.m-r-40.ng-isolate-scope > input').click() //clicking colostomy for ostomy type
    });
    And('I input cm for stoma diameter', () => {
        cy.get('[name="SOOELIMINATION0068"]').type('12') //inputting cm for stoma diameter
    });
    And('I click healed for ostomy wound', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(4) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking healed for ostomy wound
    });
    
    And('I select all for care done by for Lower GI Ostomy', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(1) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(2) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(3) > input').click()
   });
    And('I input observation and intervention12', () => {
        cy.get('[name="SOOELIMINATION0227"]').type('Test') //inputting observation
        cy.get('[name="SOOELIMINATION0211"]').type('Test') //inputting intervention
   });
    And('I click 0 for M1620', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset:nth-child(20) > table:nth-child(1) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(1) > label > input').click()
    });
        And('I click save button', () => {
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() // clicking save button
        cy.wait(5000)
    });
    //  NUTRITION/ELIMINATION TAB End -------------

    Given('I Login5', () => {
        cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    And('I click discharge tab', () => {
        cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(3) > a').click() //clicking Discharged Tab
        cy.wait(5000)
    });
    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jerwin P. Jr')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(10000)
    });

    And('I click OASIS - Discharge from Agency', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(3) > td:nth-child(2)').click();//click Discharge from Agency
        cy.wait(5000)
    });

    And('I click Edit Button', () => {
        cy.get('.btn__warning').click();//click Edit Button
        cy.wait(5000)
    });

    And('I click Neurologic or Behavioral Tab', () => {
        cy.get('#oasis-tabs > label:nth-child(6)').click()
        cy.wait(5000)
    });

    // NEUROLOGIC/BEHAVIORAL TAB Start -------------
    //Neurological Status
    And('I click left greater than right for size manual', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td.b-b-n.b-t-n.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left>right for size unequal
    });
    And('I click left for non-reactive', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(4) > td.b-t-n.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for non-reactive
    });

    //Mental status (select all)
    And('I select all for mental status', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > span > label:nth-child(1) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > span > label:nth-child(2) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > span > label:nth-child(3) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > span > label:nth-child(4) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td.p-b-5.ng-isolate-scope > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(4) > td:nth-child(1) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(4) > td.p-b-5.ng-isolate-scope > label > input').click()
    });
    And('I click adequate MD notified for sleep or rest', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td > label > input').click() //clicking adequate for sleep/rest
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td > span:nth-child(4) > label > input').click() //clicking MD notified for sleep/rest
    });
    
    //Hand grips
    And('I click left and right for strong', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td.b-b-n.b-t-n > label:nth-child(2) > input').click() //clicking left for strong
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td.b-b-n.b-t-n > label:nth-child(3) > input').click() //clicking right for strong
    });
    And('I click left and right for weak', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(8) > td > label:nth-child(2) > input').click() //clicking left for weak
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(8) > td > label:nth-child(3) > input').click() //clicking right for weak
    });
    
    //Other signs (select all)
    And('I select all for other signs', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(1) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(2) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(3) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(4) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(5) > input').type('Test') //inputting other test
    });
    
    //Weakness
    And('I click left and right for upper extremity Weakness', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > label:nth-child(2) > input').click() //clicking left for Upper extremity
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > label:nth-child(3) > input').click() //clicking right for Upper extremity
    });
    And('I click left and right for lower extremity Weakness', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label:nth-child(2) > input').click() //clicking left for Lower extremity
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label:nth-child(3) > input').click() //clicking right for Lower extremity
    });
    And('I click left for hemiparesis', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td > label:nth-child(2) > input').click() //clicking left for Hemiparesis 
    });
    
    //Paralysis
    And('I click left for hemiplegia and paraplegia', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(11) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > label:nth-child(1) > input').click() //clicking left for hemiplegia
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(11) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label > input').click() //clicking left for Paraplegia
    });
    
    //Tremors
    And('I click left, right and fine for upper extremity Tremors', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > label:nth-child(1) > input').click() //clicking left for Upper extremity
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > label:nth-child(2) > input').click() //clicking right for Upper extremity
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label:nth-child(1) > input').click() //clicking fine for Upper extremity
    });
    And('I click left, right and fine for lower extremity Tremors', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > label:nth-child(1) > input').click() //clicking left for Lower extremity
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > label:nth-child(2) > input').click() //clicking right for Lower extremity
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label:nth-child(1) > input').click() //clicking fine for Lower extremity
    });

    //Seizure
    And('I click grand mal', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(13) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > label:nth-child(1) > input').click() //clicking grand mal
    });
    And('I input date of last Seizure', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(13) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label.ng-isolate-scope > input').type('12/06/2021') //inputting date of last seizure
    });
    And('I input duration', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(13) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label.m-l-10 > input').type('50') //inputting duration (in seconds)
    });
    And('I input observation and intervention', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(14) > td:nth-child(2) > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(15) > td:nth-child(2) > input').type('Test') //inputting intervention
    });
    And('I click 1 for M1700', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
    });
    And('I click 1 for M1710', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(2) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
    });
    And('I click 2 for M1720', () => {
        cy.get(':nth-child(3) > :nth-child(1) > .molocked > :nth-child(2) > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(3) > .radio > .ng-pristine').click()
    });
    And('I click 2 for M1740', () => {
        cy.get('.molocked > .oasis__answer > .inner__table-nb > tbody > :nth-child(2) > td > .m-b-5 > .checkbox > .ng-pristine').click()
    });
    And('I click 2 for M1745', () => {
        cy.get(':nth-child(5) > :nth-child(1) > .molocked > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(3) > .radio > .ng-pristine').click()
    });
    //Thought Process, Affect and Behavioral Status (select all)
    And('I select all for Thought Process, Affect and Behavioral Status', () => {
        cy.get('[rname="SOONEURO0062"]').click({multiple:true})
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > input').type('Test') //inputting other in Thought Process, Affect and Behavioral Status
   });

    And('I click save button', () => {
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() // clicking save button
        cy.wait(5000)
    });
    // NEUROLOGIC/BEHAVIORAL TAB End -------------

    Given('I Login7', () => {
        cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    And('I click discharge tab', () => {
        cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(3) > a').click() //clicking Discharged Tab
        cy.wait(5000)
    });
    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jerwin P. Jr')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(10000)
    });

    And('I click OASIS - Discharge from Agency', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(3) > td:nth-child(2)').click();//click Discharge from Agency
        cy.wait(5000)
    });

    And('I click Edit Button', () => {
        cy.get('.btn__warning').click();//click Edit Button
        cy.wait(5000)
    });

    And('I click ADL or IADL or Musculoskeletal Tab', () => {
        cy.get('#oasis-tabs > label:nth-child(7)').click()
        cy.wait(5000)
    });

    // ADL/IADL/Musculoskeletal TAB Start -------------
    //Musculoskeletal Status
    And('I click strong for muscle strength', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.p-5.b-l-n > label > input').click() //clicking strong for muscle strength
    });
    And('I click limited for Range of motion', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > label > input').click() //clicking limited for Range of motion\
    });

    //limited (select all)
    And('I select all for limited', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > span > label:nth-child(2) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > span > label:nth-child(3) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > span > label:nth-child(4) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > span > label:nth-child(5) > input').click()
    });
    And('I click independent for Bed mobility', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td.b-l-n.ng-isolate-scope > label > input').click() //clicking independent for Bed mobility
    });
    And('I click independent for Transfer ability', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td.b-l-n.ng-isolate-scope > label > input').click() //clicking independent for Transfer ability
    });
    And('I click steady for Gait or Ambulation', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td.b-l-n > label > input').click() //clicking steady for Gait/Ambulation
    });
    And('I click good for balance', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td.b-l-n > label > input').click() //clicking good for balance
    });
    And('I input seconds for Timed Up & Go', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.b-l-n.ng-isolate-scope > span.m-l-5 > input').type('10') //inputting seconds for Timed Up & Go
    });
    And('I click practiced once before actual test and unable to perform for Timed Up & Go', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.b-l-n.ng-isolate-scope > label:nth-child(3) > input').click() //clicnking Practiced once before actual test for Timed Up & Go
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.b-l-n.ng-isolate-scope > label:nth-child(4) > input').click() //clicnking Unable to perform for Timed Up & Go
    });
    And('I click low for risk for falls', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(8) > td.b-l-n > label > input').click() //clicking low for risk for falls
    });
    And('I click left and right for amputation', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for amputation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(2) > input').click() //clicking right for amputation
    });
    And('I click BK, AK, UE and input other for amputation', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(3) > input').click() //clicking BK
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(4) > input').click() //clicking AK
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(5) > input').click() //clicking UE
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > input').type('Test') //inputting other for amputation
    });
    And('I click new, input location and click cast for fracture', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(10) > td.b-l-n.ng-isolate-scope > label:nth-child(1) > input').click() //clicking new for fracture
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(10) > td.b-l-n.ng-isolate-scope > input').type('Test') //inputting location for fracture
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(10) > td.b-l-n.ng-isolate-scope > label.checkbox.checkbox-inline.m-l-5.ng-isolate-scope > input').click() //clicking cast?
    });
    And('I input observation and intervention', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(11) > td.p-l-5.p-r-5 > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(12) > td.p-l-5.p-r-5 > input').type('Test') //inputting intervention
    });

    //If cast is present, assessment of extremity distal to cast
    And('I click pink for color', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking pink for color
    });
    And('I click strong for Pulses', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking strong for pulses
    });
    And('I click < 3 sec for Capillary refill', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > label.radio.radio-inline.m-l-5.ng-isolate-scope > input').click() //clicking < 3 sec for Capillary refill	
    });
    And('I click warm for temperature', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking warm for temperature
    });
    And('I click normal for sensation', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(4) > label:nth-child(1) > input').click() //clicking normal for sensation
    });
    And('I click able to move for motor function', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(4) > label:nth-child(1) > input').click() //clicking able to move for motor function
    });
    And('I click yes for dwelling', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(4) > label:nth-child(1) > input').click() //clicking yes for swelling
    });
    And('I input intervention', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > input').type('Test') //inputting for intervention
    });

    // Functional Limitations (select all)
    And('I select all functional Limitations', () => {
        cy.get('[rname="SOOADL0058"]').click({multiple:true})
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other for functional limitations
    });

    //Activities Permitted (select all)
    And('I select all activities Permitted', () => {
        cy.get('[rname="SOOADL0060"]').click({multiple:true})
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(4) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting others for activities permitted
    });
    And('I click 2 for M1800 - Grooming', () => {
        //(M1800) - Grooming
        cy.get('[ui-view=""] > :nth-child(6) > :nth-child(1) > .molocked > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(3) > .radio > .ng-pristine').click()
    });
    And('I click 1 for M1810 - Current Ability to Dress Upper Body', () => {
        //(M1810) - Current Ability to Dress Upper Body
        cy.get('[ng-if="!(subsetPreloader && tab.preloader)"][style=""] > :nth-child(1) > .molocked > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(2) > .radio > .ng-pristine').click()
    });
    And('I click 1 for M1820 - Current Ability to Dress Lower Body', () => {
        //(M1820) - Current Ability to Dress Lower Body
        cy.get(':nth-child(2) > :nth-child(1) > .molocked > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(2) > .radio > .ng-valid').click()
    });
    And('I click 2 for M1830 - Bathing', () => {
        //(M1830) - Bathing
        cy.get(':nth-child(3) > :nth-child(1) > .molocked > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(3) > .radio > .ng-pristine').click()
    });
    And('I click 1 for M1840 - Toilet Transferring', () => {
        //(M1840) - Toilet Transferring
        cy.get(':nth-child(4) > :nth-child(1) > .molocked > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(2) > .radio > .ng-pristine').click()
    });
    And('I click 1 for M1845 - Toileting Hygiene', () => {
        //(M1840) - Toilet Transferring
        cy.get('fieldset.ng-scope > :nth-child(5) > :nth-child(1) > .molocked > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(2) > .radio > .ng-pristine').click()
    });

    And('I click 1 for M1850 - Transferring', () => {
        //(M1850) - Transferring
        cy.get('fieldset.ng-scope > :nth-child(6) > :nth-child(1) > .molocked > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(2) > .radio > .ng-valid').click()
    });
    And('I click 1 for M1860 - Ambulation or Locomotion', () => {
        //(M1860) - Ambulation/Locomotion
        cy.get(':nth-child(7) > :nth-child(1) > .molocked > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(2) > .radio > .ng-valid').click()
    });
    And('I click 1 for M1870 - Feeding or Eating', () => {
        //(M1870) - Feeding or Eating
        cy.get(':nth-child(8) > :nth-child(1) > .molocked > .oasis__answer > .inner__table-nb > tbody > tr > .ng-isolate-scope > :nth-child(2) > .radio > .ng-valid').click()
    });
    //Section GG Functional Abilities and Goals
    //GG0130. Self‐Care
    And('I select 6 on A - Eating', () => {
        cy.get('#GG0130A3_chosen > a').click() //clicking input box of A for Discharge Performance
        cy.get('#GG0130A3_chosen > div > ul > li:nth-child(1)').click() //clicking result
    });
    And('I select 5 on B - Oral Hygiene', () => {
        cy.get('#GG0130B3_chosen > a').click() //clicking input box of B for Discharge Performance
        cy.get('#GG0130B3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on C - Toileting Hygiene', () => {
        cy.get('#GG0130C3_chosen > a').click() //clicking input box of C for Discharge Performance
        cy.get('#GG0130C3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on E - Shower or bathe self', () => {
        cy.get('#GG0130E3_chosen > a').click() //clicking input box of E for Discharge Performance
        cy.get('#GG0130E3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on F - Upper body dressing', () => {
        cy.get('#GG0130F3_chosen > a').click() //clicking input box of F for Discharge Performance
        cy.get('#GG0130F3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on G - Lower body dressing', () => {
        cy.get('#GG0130G3_chosen > a').click() //clicking input box of G for Discharge Performance
        cy.get('#GG0130G3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on H - Putting on or taking off footwear', () => {
        cy.get('#GG0130H3_chosen > a').click() //clicking input box of H for Discharge Performance
        cy.get('#GG0130H3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    //GG0170. Mobility
    And('I select 5 on A - Roll left and right for Discharge Performance', () => {
        cy.get('#GG0170A3_chosen > a').click() // clicking input box of A for Discharge Performance
        cy.get('#GG0170A3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on B - sit to lying for Discharge Performance', () => {
        cy.get('#GG0170B3_chosen > a').click() // clicking input box of B for Discharge Performance
        cy.get('#GG0170B3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on C - lying to sitting on side of bed for Discharge Performance', () => {
        cy.get('#GG0170C3_chosen > a').click() // clicking input box of C for Discharge Performance
        cy.get('#GG0170C3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on D - sit to stand for Discharge Performance', () => {
        cy.get('#GG0170D3_chosen > a').click() // clicking input box of D for Discharge Performance
        cy.get('#GG0170D3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on E - Chair or bed-to-chair transfer for Discharge Performance', () => {
        cy.get('#GG0170E3_chosen > a').click() // clicking input box of E for Discharge Performance
        cy.get('#GG0170E3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on F - toilet transfer for Discharge Performance', () => {
        cy.get('#GG0170F3_chosen > a').click() // clicking input box of F for Discharge Performance
        cy.get('#GG0170F3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on G - Car transfer for Discharge Performance', () => {
        cy.get('#GG0170G3_chosen > a').click() // clicking input box of G for Discharge Performance
        cy.get('#GG0170G3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 6 on I - Walk ten feet for Discharge Performance', () => {
        cy.get('#GG0170I3_chosen > a').click() // clicking input box of I for Discharge Performance
        cy.get('#GG0170I3_chosen > div > ul > li.active-result.highlighted').click() //clicking result
    });
    And('I select 5 on J - walk fifty feet with two turns for Discharge Performance', () => {
        cy.get('#GG0170J3_chosen > a').click() // clicking input box of J for Discharge Performance
        cy.get('#GG0170J3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on K - one-fifty feet for Discharge Performance', () => {
        cy.get('#GG0170K3_chosen > a').click() // clicking input box of K for Discharge Performance
        cy.get('#GG0170K3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on L - walking ten feet on uneven surfaces for Discharge Performance', () => {
        cy.get('#GG0170L3_chosen > a').click() // clicking input box of L for Discharge Performance
        cy.get('#GG0170L3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 6 on M - one step - curb for Discharge Performance', () => {
        cy.get('#GG0170M3_chosen > a').click() // clicking input box of M for Discharge Performance
        cy.get('#GG0170M3_chosen > div > ul > li.active-result.highlighted').click() //clicking result
    });
    And('I select 5 on N - four steps for Discharge Performance', () => {
        cy.get('#GG0170N3_chosen > a').click() // clicking input box of N for Discharge Performance
        cy.get('#GG0170N3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on O - Twelve steps for Discharge Performance', () => {
        cy.get('#GG0170O3_chosen > a').click() // clicking input box of O for Discharge Performance
        cy.get('#GG0170O3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on P - Picking up object for Discharge Performance', () => {
        cy.get('#GG0170P3_chosen > a').click() // clicking input box of P for Discharge Performance
        cy.get('#GG0170P3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 1-yes on Q - Does patient use wheelchair and or scooter', () => {
        cy.get('#GG0170Q3_chosen > a').click() // clicking input box for Q
        cy.get('#GG0170Q3_chosen > div > ul > li:nth-child(2)').click() // clicking result
    });
    And('I select 5 on R - wheel fifty feet with two turns for Discharge Performance', () => {
        cy.get('#GG0170R3_chosen > a').click() // clicking input box of R for Discharge Performance
        cy.get('#GG0170R3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 2 on RR3 - Indicate the type of wheelchair or scooter used', () => {
        cy.get('#GG0170RR3_chosen > a').click() // clicking input box of RR3 for Discharge Performance
        cy.get('#GG0170RR3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 5 on S - wheel one-fifty feet for Discharge Performance', () => {
        cy.get('#GG0170S3_chosen > a').click() // clicking input box of S for Discharge Performance
        cy.get('#GG0170S3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I select 2 on SS3 - Indicate the type of wheelchair or scooter used', () => {
        cy.get('#GG0170SS3_chosen > a').click() // clicking input box of SS3 for Discharge Performance
        cy.get('#GG0170SS3_chosen > div > ul > li:nth-child(2)').click() //clicking result
    });

    And('I click save button', () => {
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() // clicking save button
        cy.wait(5000)
    });
    // ADL/IADL/Musculoskeletal TAB End -------------

    Given('I Login8', () => {
        cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    And('I click discharge tab', () => {
        cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(3) > a').click() //clicking Discharged Tab
        cy.wait(5000)
    });
    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jerwin P. Jr')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(10000)
    });

    And('I click OASIS - Discharge from Agency', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(3) > td:nth-child(2)').click();//click Discharge from Agency
        cy.wait(5000)
    });

    And('I click Edit Button', () => {
        cy.get('.btn__warning').click();//click Edit Button
        cy.wait(5000)
    });

    And('I click Medication Tab', () => {
        cy.get('#oasis-tabs > label:nth-child(8)').click()
        cy.wait(5000)
    });

    // MEDICATION TAB Start -------------
    And('I click 1 - yes for M2005', () => {
        //(M2005) - Medication Intervention: Did the agency contact and complete physician (or physician-designee) prescribed/recommended actions by midnight of the next calendar day each time potential clinically significant medication issues were identified since the SOC/ROC?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click() //clicking 1-Yes
    });
    And('I click 1 - yes for M2016', () => {
        //(M2016) - Patient/Caregiver Drug Education Intervention: At the time of, or at any time since the most recent SOC/ROC assessment, was the patient/caregiver instructed by agency staff or other health care provider to monitor the effectiveness of drug therapy, adverse drug reactions, and significant side effects, and how and when to report problems that may occur?
        cy.get('#M2016_DRUG_EDCTN_INTRVTN > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click() //clicking 1-Yes
    });
    And('I click 1 for M2020', () => {
        cy.get('#M2020_CRNT_MGMT_ORAL_MDCTN > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
    });
    And('I click save button', () => {
        //SAVE BUTTON
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
        cy.wait(5000)    
    });   
    // MEDICATION TAB End -------------


    Given('I Login9', () => {
        cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    And('I click discharge tab', () => {
        cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(3) > a').click() //clicking Discharged Tab
        cy.wait(5000)
    });
    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jerwin P. Jr')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(10000)
    });

    And('I click OASIS - Discharge from Agency', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(3) > td:nth-child(2)').click();//click Discharge from Agency
        cy.wait(5000)
    });

    And('I click Edit Button', () => {
        cy.get('.btn__warning').click();//click Edit Button
        cy.wait(5000)
    });


    And('I click Care Management Tab', () => {
        cy.get('#oasis-tabs > label:nth-child(9)').click()
        cy.wait(5000)
    });

    // CARE MANAGEMENT TAB Start -------------
    And('I click caregiver always available and reliable for caregiving status', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking Caregiver always available and reliable for caregiving status
    });
    And('I input caregiver name, relationship, phone, name of facility, phone for facility, contact person and community resources utilized', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > input').type('Test') //inputting caregiver name
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > input').type('Test') //inputting relationship
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > input').type('1234567890') //inputting phone
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > input').type('Test') //inputting name of facility
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > input').type('1234567890') //inputting phone
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > input').type('Test') //inputting contact person
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > div > input').type('Test') //inputting Community resources utilized
    });
    //(M2102) - Types and Sources of Assistance
    And('I click 1 for ADL assistance', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
    });
    And('I click 1 for Medication administration', () => {
        cy.get('#M2102_CARE_TYPE_SRC_MDCTN > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
    });
    And('I click 1 for Medical procedures or treatments', () => {
        cy.get('#M2102_CARE_TYPE_SRC_PRCDR > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
    });
    And('I click 1 for Supervision and safety', () => {
        cy.get('#M2102_CARE_TYPE_SRC_SPRVSN > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
    });
    And('I click save button', () => {
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() // clicking save button
        cy.wait(5000)
    });
    // CARE MANAGEMENT TAB End -------------

    Given('I Login10', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });
    And('I click discharge tab', () => {
        cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(3) > a').click() //clicking Discharged Tab
        cy.wait(5000)
    });
    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jerwin P. Jr')  //search the added patient 
        cy.wait(5000) 
    });
    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(10000)
    });
    And('I click OASIS - Discharge from Agency', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(3) > td:nth-child(2)').click();//click Discharge from Agency
        cy.wait(5000)
    });
    And('I click Edit Button', () => {
        cy.get('.btn__warning').click();//click Edit Button
        cy.wait(5000)
    });
    And('I click Emergent Care Tab', () => {
        cy.get('#oasis-tabs > label:nth-child(10)').click() //clicking medication tab
    });

    //EMERGENT CARE TAB Start -------------
    And('I click Yes, used hospital emergency department WITHOUT hospital admission for M2301', () => {
        cy.get('#M2301_EMER_USE_AFTR_LAST_ASMT > td.oasis__answer.v-top > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
    });
    And('I select all that apply for M2310', () => {
        cy.get('#M2310_ECR_MEDICATION').click()
        cy.get('#M2310_ECR_HYPOGLYC').click()
        cy.get('#M2310_ECR_OTHER').click()
    });
    //DATA ITEMS COLLECTED AT INPATIENT FACILITY ADMISSION OR AGENCY DISCHARGE ONLY
    //(M2401) - Intervention Synopsis (clicking yes to all)
    And('I click yes to all Intervention Synopsis', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(5) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(6) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(7) > td:nth-child(3) > div > label > input').click()
    });
    And('I click 1 - Hospital for M2410', () => {
        // /(M2410) - To which Inpatient Facility has the patient been admitted?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(9) > td.oasis__answer.v-top > table > tbody > tr > td > div > div:nth-child(1) > label > input').click() //clicking 1 - Hospital
    });
    And('I click 1 - Hospital for M2420', () => {
        // /(M2420) - Discharge Disposition
        cy.get(':nth-child(11) > .oasis__answer > .inner__table-nb > tbody > tr > .oasis-w-40 > .ng-isolate-scope > :nth-child(1) > .radio > .ng-pristine').click()
    });

    And('I input Discharge-Transfer-Death Date', () => {
        //(M0906) - Discharge/Transfer/Death Date
        cy.get('#M0906_DC_TRAN_DTH_DT_DATEPICKER').type(dayjs().format('MM/DD/YYYY'));
    });
    //Section J: Health Conditions
    //J1800 - Any Falls Since SOC/ROC
    And('I click yes for J1800', () => {
        cy.get(':nth-child(16) > .oasis__answer > .inner__table-nb > tbody > tr > .oasis-w-40 > .ng-isolate-scope > :nth-child(2) > .radio > .ng-valid').click() //clicking 1-YES
    });
    //J1900 - Number of Falls Since SOC/ROC
    And('I input 1 for A - No Injury', () => {
        cy.get('[data-ng-model="data.J1900A"]').type('1')
    });
    And('I input 1 for B - Injury', () => {
        cy.get('#J1900B').type('1') //inputting 1 B. Injury (except major)
    });
    And('I input 1 for C - Major Injury', () => {
        cy.get('#J1900C').type('1') //inputting 1 C. Major injury
    });
    And('I input additional assessment', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(20) > td:nth-child(2) > div > div > textarea').type('Test')
    });
    And('I click all reasons for discharge', () => {
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(1) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(2) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(3) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(4) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(5) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(6) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(7) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(2) > div:nth-child(1) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(2) > div:nth-child(2) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(2) > div:nth-child(3) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(2) > div:nth-child(4) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(2) > div:nth-child(5) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(2) > div:nth-child(6) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other for reasons for discharge
    });
    And('I click save button', () => {
        //SAVE BUTTON
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
        cy.wait(5000)    
    });

    //EMERGENT CARE TAB End -------------

    












    

