Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });

  const dayjs = require('dayjs')
  
  
  Given('I Login', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Will, Sumera P. Jr.')  //search the added patient 
        cy.wait(10000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(10000)
        cy.scrollTo('bottom')
    });
    And('I click CHHA - HHA Visit', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(18) > td:nth-child(2)').click();//click CHHA - HHA Visit
        cy.wait(5000)
    });
    And('I click Edit Button', () => {
        cy.get('.btn__warning').click();//click Edit Button
        cy.wait(5000)
    });
    And('I input time in and time out', () => {
        cy.get('#timeIn').type('1200') //inputting time in
        cy.get('#timeOut').type('1900') //inputting time out
    });
    //VITAL SIGNS PARAMETERS
    And('I input for greater than SBP, DBP, Temp, Pulse-Radial and Resp', () => {
        cy.get(':nth-child(2) > tbody > :nth-child(3) > :nth-child(2) > .global__txtbox').type('3') //inputting Greater than > SBP
        cy.get(':nth-child(2) > tbody > :nth-child(3) > :nth-child(3) > .global__txtbox').type('3') //inputting Greater than > DBP
        cy.get(':nth-child(2) > tbody > :nth-child(3) > :nth-child(4) > .global__txtbox').type('3') //inputting Greater than > Temp
        cy.get(':nth-child(2) > tbody > :nth-child(3) > :nth-child(5) > .global__txtbox').type('3') //inputting Greater than > Pulse (Radial)	
        cy.get(':nth-child(2) > tbody > :nth-child(3) > :nth-child(6) > .global__txtbox').type('3') //inputting Greater than > Resp
    });
    And('I input for less than SBP, DBP, Temp, Pulse-Radial and Resp', () => {
        cy.get(':nth-child(4) > :nth-child(2) > .global__txtbox').type('3') //inputting Less than < SBP
        cy.get(':nth-child(4) > :nth-child(3) > .global__txtbox').type('3') //inputting Less than < DBP
        cy.get(':nth-child(4) > :nth-child(4) > .global__txtbox').type('3') //inputting Less than < Temp
        cy.get(':nth-child(4) > :nth-child(5) > .global__txtbox').type('3') //inputting Less than < Pulse (Radial)	
        cy.get(':nth-child(4) > :nth-child(6) > .global__txtbox').type('3') //inputting Less than < Resp
    });
    //VITAL SIGNS
    And('I input blood pressure, temp, pulse-radial, resp, last BM and weight', () => {
        cy.get(':nth-child(3) > :nth-child(1) > .global__txtbox').type('180/120') //inputting Blood Pressure
        cy.get(':nth-child(3) > :nth-child(1) > :nth-child(3) > :nth-child(2) > .global__txtbox').type('120') //inputting Temp
        cy.get(':nth-child(3) > :nth-child(1) > :nth-child(3) > :nth-child(3) > .global__txtbox').type('120') //inputting Pulse (Radial)
        cy.get(':nth-child(3) > :nth-child(1) > :nth-child(3) > :nth-child(4) > .global__txtbox').type('120') //inputting Resp
        cy.get(':nth-child(3) > :nth-child(1) > :nth-child(3) > :nth-child(5) > .global__txtbox').type('120') //inputting Last BM
        cy.get(':nth-child(3) > :nth-child(1) > :nth-child(3) > :nth-child(6) > .global__txtbox').type('120') //inputting Weight
    });
    //Pain
    And('I click denies pain', () => {
        cy.get('[colspan="6"] > table > tbody > tr > :nth-child(2) > .m-tb-0 > .radio > .ng-valid').click() //clicking denies pain
    });
    And('Input pain location', () => {
        cy.get('.cont-opt > .global__txtbox').type('Test') //inputting location
    });
    And('I click 1 for present level', () => {
        cy.get(':nth-child(7) > .m-tb-0 > .radio > .ng-valid').click() //clicking 1 present level
    });
    //TASK
    //Personal
    And('I click completed for Bed bath, Chair bath, Shower, Tub Bath, Hair shampoo and Hair care or comb hair', () => {
        cy.get(':nth-child(4) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Bed bath
        cy.get(':nth-child(5) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Chair bath
        cy.get(':nth-child(6) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Shower
        cy.get(':nth-child(8) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Tub Bath
        cy.get(':nth-child(9) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Hair shampoo
        cy.get(':nth-child(11) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Hair care/comb hair
    });
    And('I click completed for Oral care or comb dentures, Skin or Foot care, Shave or Groom, Nail care, Perineal care and Assist with dressing', () => {
        cy.get(':nth-child(12) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Oral care/comb dentures
        cy.get(':nth-child(14) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Skin/Foot care
        cy.get(':nth-child(15) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Shave/Groom
        cy.get(':nth-child(17) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Nail care (Clean/File)
        cy.get(':nth-child(18) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Perineal care
        cy.get(':nth-child(19) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Assist with dressing
    });
    //Nutrition
    And('I click completed for Meal preparation and Assist with feeding', () => {
        cy.get(':nth-child(21) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Meal preparation
        cy.get(':nth-child(22) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Assist with feeding
    });
    And('I click Encourage', () => {
        cy.get(':nth-child(23) > :nth-child(1) > :nth-child(1) > .ng-pristine').click() //clicking Encourage
    });
    And('I click completed for Fluid intake', () => {
        cy.get(':nth-child(23) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Fluid intake
    });
    //Household Tasks
    And('I click completed for Light housekeeping, Change bed linen, Wash clothes and Assist with errands', () => {
        cy.get(':nth-child(25) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Light housekeeping
        cy.get(':nth-child(26) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Change bed linen
        cy.get(':nth-child(27) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Wash clothes
        cy.get(':nth-child(28) > :nth-child(2) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Assist with errands
    });
    //Activities
    And('I click completed for Repositioning and turing in bed', () => {
        cy.get(':nth-child(4) > :nth-child(5) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Repositioning and turing in bed
    });
    //Assist with mobility/transfer (click all)
    And('I click all Assist with mobility or transfer', () => {
        cy.get(':nth-child(5) > [rowspan="2"] > table > tbody > [style="height: 25px;"] > :nth-child(1) > .checkbox > .p-l-25 > .ng-valid').click()
        cy.get(':nth-child(3) > :nth-child(1) > .checkbox > .p-l-25 > .ng-valid').click()
        cy.get(':nth-child(5) > [rowspan="2"] > table > tbody > [style="height: 25px;"] > :nth-child(2) > .checkbox > .p-l-25 > .ng-valid').click()
        cy.get(':nth-child(3) > :nth-child(2) > .checkbox > .p-l-25 > .ng-valid').click()
        cy.get(':nth-child(5) > [rowspan="2"] > table > tbody > [style="height: 25px;"] > :nth-child(3) > .checkbox > .p-l-25 > .ng-valid').click()
    });
    And('I click completed for Assist with mobility or transfer', () => {
        cy.get(':nth-child(5) > :nth-child(5) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Assist with mobility/transfer
    });
    //Assist in ambulation (click all)
    And('I click all for Assist in ambulation', () => {
        cy.get(':nth-child(1) > .checkbox > .m-r-10 > .ng-pristine').click()
        cy.get(':nth-child(2) > .checkbox > .m-r-10 > .ng-pristine').click()
        cy.get(':nth-child(3) > .checkbox > .m-r-10 > .ng-pristine').click()
    });
    And('I click completed for Assist in ambulation', () => {
        cy.get(':nth-child(8) > :nth-child(5) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Assist in ambulation
    });
    //Range of motion (click all)
    And('I click all for range of motion', () => {
        cy.get(':nth-child(11) > [rowspan="2"] > table > tbody > [style="height: 25px;"] > :nth-child(1) > .checkbox > .p-l-25 > .ng-pristine').click()
        cy.get(':nth-child(11) > [rowspan="2"] > table > tbody > [style="height: 25px;"] > :nth-child(2) > .checkbox > .p-l-25 > .ng-pristine').click()
        cy.get(':nth-child(11) > [rowspan="2"] > table > tbody > [style="height: 25px;"] > :nth-child(3) > .checkbox > .p-l-25 > .ng-pristine').click()
        cy.get(':nth-child(11) > [rowspan="2"] > table > tbody > [style="height: 25px;"] > :nth-child(4) > .checkbox > .p-l-25 > .ng-pristine').click()
    });
    And('I click completed for range of motion', () => {
        cy.get(':nth-child(11) > :nth-child(5) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Range of motion
    });
    //Assist in prescribed exercises per (click all)
    And('I click all for Assist in prescribed exercises per', () => {
        cy.get(':nth-child(14) > [rowspan="2"] > table > tbody > [style="height: 25px;"] > :nth-child(1) > .checkbox > .p-l-25 > .ng-pristine').click()
        cy.get(':nth-child(14) > [rowspan="2"] > table > tbody > [style="height: 25px;"] > :nth-child(2) > .checkbox > .p-l-25 > .ng-pristine').click()
        cy.get(':nth-child(14) > [rowspan="2"] > table > tbody > [style="height: 25px;"] > :nth-child(3) > .checkbox > .p-l-25 > .ng-pristine').click()
        cy.get(':nth-child(14) > [rowspan="2"] > table > tbody > [style="height: 25px;"] > :nth-child(4) > .checkbox > .p-l-25 > .ng-pristine').click()
    });
    And('I click completed for Assist in prescribed exercises per and Equipment care', () => {
        cy.get(':nth-child(14) > :nth-child(5) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Assist in prescribed exercises per
        cy.get(':nth-child(17) > :nth-child(5) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Equipment care
    });
    //Procedures
    And('I click completed for Medical reminder, Assist with elimination, Incontinence care, Record bowel movement, Urinary catheter care, Record intake and output, Ostomy care and Empty drainage bag', () => {
        cy.get(':nth-child(19) > :nth-child(5) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Medical reminder
        cy.get(':nth-child(20) > .table-nbr > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Assist with elimination
        cy.get(':nth-child(21) > :nth-child(5) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Incontinence care
        cy.get(':nth-child(22) > :nth-child(5) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Record bowel movement
        cy.get(':nth-child(23) > :nth-child(5) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Urinary catheter care
        cy.get(':nth-child(24) > .table-nbr > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Record intake and output
        cy.get(':nth-child(25) > :nth-child(5) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Ostomy care
        cy.get(':nth-child(26) > :nth-child(5) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Empty drainage bag
    });
    And('I click inspect and reinforce', () => {
        cy.get('[chk="data.dressingInspect"] > .ng-pristine').click() //clicking inspect 
        cy.get('[chk="data.dressingReinforce"] > .ng-valid').click() //clicking reinforce 
    });
    And('I click completed for Wound dressing', () => {
        cy.get(':nth-child(27) > :nth-child(5) > .m-tb-0 > .p-l-25 > .ng-pristine').click() //clicking completed Wound dressing
    });
    And('I input notes', () => {
        cy.get('.table-input').type('Test') //inputting notes
    });
    And('I click save button', () => {
        cy.get('.btn__success').click() //clicking save button
        cy.wait(10000)
    });



