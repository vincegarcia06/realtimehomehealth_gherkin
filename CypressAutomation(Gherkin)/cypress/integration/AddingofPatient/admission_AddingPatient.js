Cypress.on('uncaught:exception', (err, runnable) => {
  return false;
});
let rowsLength;

Given('I Login', () => {
  cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
            cy.viewport(1920, 924);//setting your windows size
  })

  And('I visit admission page and Modal will Display', () => {
    cy.visit('https://qado.medisource.com/patient'); //visit page
    cy.wait(5000)
    cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form').should('be.exist')
  });

  When('I click skip', () => { 
        cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div > div:nth-child(3) > div > a').click()
  })
  
  Then('Modal will disappear', () => {
    cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div').should('not.be.exist')
  });

  When('I input referral date and hours, click auto assign and input planned SOC date', () => { 
    const dayjs = require('dayjs')
    cy.get('#refDate').type('05132022')
    cy.get('#referral_time > input').type('1200')
    cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > ng-form > fieldset > table > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td.global__table-question > div > label > input').click()
    cy.get('#pre_admission_date').type(dayjs().format('MM/DD/YYYY'));
  })

  And('I input in firstname, lastname, middleinitial, suffix and birthdate', () => { 
    cy.task('readXlsx', { file: 'cypress/fixtures/AddingofPatient.xlsx', sheet: "PatientInformation" }).then((rows) => {
      rowsLength = rows.length;
      cy.writeFile("cypress/fixtures/PatientInformation.json", {rows})
    })
    cy.fixture('PatientInformation').then((data) => {
     for ( let i = 0; i < rowsLength; i++) {
      cy.get('#last_name').type(data.rows[i].lastname);
      cy.get('#first_name').type(data.rows[i].firstname);
      cy.get('#mi').type(data.rows[i].middleinitial);
      cy.get('#suffix').type(data.rows[i].suffix);
      cy.get('#birthdate').type(data.rows[i].birthdate);
     }
    })
  })

    And('I check male', () =>{
        cy.get('.custom-input-radio > :nth-child(1) > .ng-pristine').check();
    })

    And('I select marital status "Single", ethnicity "Asian" and language "English"', () =>{
      cy.get('#marital_status_chosen > .chosen-single').click();
      cy.get('#marital_status_chosen > div > ul > li:nth-child(1)').click();
      cy.get('#ethnicities_chosen > .chosen-choices > .search-field > .default').click();
      cy.get('#ethnicities_chosen > div > ul > li:nth-child(1)').click();
      cy.get('#language_spoken > div:nth-child(1) > input').click()
      cy.get('#ui-select-choices-row-0-0 > span').click()
      cy.wait(5000)
    })
    And('I input Social Security Number', () =>{
      var ninenumbers = String(Math.random()).substring(2,11);
      cy.get('#ssNumber').type(ninenumbers);
  })

    And('I input streetaddress, additionaldirections, majorcrossstreet and city', () => { 
      cy.task('readXlsx', { file: 'cypress/fixtures/AddingofPatient.xlsx', sheet: "PatientAddress" }).then((rows) => {
        rowsLength = rows.length;
        cy.writeFile("cypress/fixtures/PatientAddress.json", {rows})
      })
      cy.fixture('PatientAddress').then((data) => {
       for ( let i = 0; i < rowsLength; i++) {
        cy.get('#main_line1').type(data.rows[i].streetaddress);
        cy.get('#main_line2').type(data.rows[i].additionaldirections);
        cy.get('#main_street').type(data.rows[i].majorcrossstreet);
        cy.get('#main_city').type(data.rows[i].city);
       }
      })
  })

  And('I select state', () =>{
      cy.get('#main_state_chosen > a').click();
      cy.get('#main_state_chosen > div > ul > li:nth-child(6)').click();
  })


  And('I input zipcode, phone1, phone2 and email', () => { 
    cy.fixture('PatientAddress').then((data) => {
      for ( let i = 0; i < rowsLength; i++) {
    cy.get('#main_state_chosen > a').click();
    cy.get('#main_state_chosen > div > ul > li:nth-child(6)').click();
    cy.get('#main_zipcode').type(data.rows[i].zipcode);
    cy.get(':nth-child(22) > .oasis__answer > .fg-line > .global__txtbox').type(data.rows[i].phone1);
    cy.get(':nth-child(23) > .oasis__answer > .fg-line > .global__txtbox').type(data.rows[i].phone2);
    cy.get('#main_email').type(data.rows[i].email);
      }
    })
  })

  And('I click same as patient address button', () =>{
      cy.get(':nth-child(25) > [colspan="2"] > .btn__dark-blue_icon-bordered').click();//clicking Same as Patient Address button
  })

  And('I input caregiver name and phone', () => { 
    cy.task('readXlsx', { file: 'cypress/fixtures/AddingofPatient.xlsx', sheet: "LivingSituation" }).then((rows) => {
      rowsLength = rows.length;
      cy.writeFile("cypress/fixtures/LivingSituation.json", {rows})
    })
    cy.fixture('LivingSituation').then((data) => {
     for ( let i = 0; i < rowsLength; i++) {
      cy.get('#ls_caregiver').type(data.rows[i].caregivername);
      cy.get('#ls_caregiver_phone').type(data.rows[i].phone)
     }
    })
  })

  And('I input name, relationship, emergencyphone1 and emergencyphone2 in emergency contact', () => { 
    cy.task('readXlsx', { file: 'cypress/fixtures/AddingofPatient.xlsx', sheet: "EmergencyContact" }).then((rows) => {
      rowsLength = rows.length;
      cy.writeFile("cypress/fixtures/EmergencyContact.json", {rows})
    })
    cy.fixture('EmergencyContact').then((data) => {
     for ( let i = 0; i < rowsLength; i++) {
      cy.get('#ec_name').type(data.rows[i].name);
      cy.get('#ec_relationship').type(data.rows[i].relationship);
      cy.get('#ec_phone').type(data.rows[i].emergencyphone1);
      cy.get('#ec_other_phone').type(data.rows[i].emergencyphone2);
     }
    })
  })

  And('I select attending physician, primary care physician and other physician', () =>{
      cy.get('#physician_attending_chosen > .chosen-single').click();
      cy.get('#physician_attending_chosen > div > ul > li:nth-child(2)').click();
      cy.get('#physician_primary_chosen > .chosen-single').click();
      cy.get('#physician_primary_chosen > div > ul > li:nth-child(2)').click();
      cy.get('[style="height: unset;"] > .select > .chosen-container > .chosen-choices').click();//other physician
      cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > ng-form > fieldset > table > tbody > tr:nth-child(46) > td:nth-child(2) > div > div > div > ul > li:nth-child(2)').click();
  })

  And('I select type and input policy or HIC number for primary insurance', () =>{
      cy.get('#primary_insurance_chosen > .chosen-single').click();
      cy.get('#primary_insurance_chosen > div > ul > li:nth-child(2)').click();
      cy.get('#primary_insurance_policy').type('qwerty45678');
  })

  And('I select type and input policy or HIC number for secondary insurance', () =>{
      cy.get('#secondary_insurance_chosen > .chosen-single').click();
      cy.get('#secondary_insurance_chosen > div > ul > li:nth-child(2)').click();
      cy.get('#secondary_insurance_policy').type('qwerty1234');
  })

  And('I select type of admission, point of origin, type of referral and referral source for referral information', () =>{
      cy.get('#admissiontype_chosen > .chosen-single').click();
      cy.get('#admissiontype_chosen > div > ul > li:nth-child(1)').click();
      cy.get('#point_of_origin_chosen > .chosen-single').click();
      cy.get('#point_of_origin_chosen > div > ul > li:nth-child(1)').click();
      cy.get('#referral_type_chosen > .chosen-single').click();
      cy.get('#referral_type_chosen > div > ul > li:nth-child(3)').click();
      cy.get('#referral_source_id_chosen > .chosen-single').click();
      cy.get('#referral_source_id_chosen > div > ul > li:nth-child(2)').click();
  })

  And('I select hospital or facility for hospitalization information', () =>{
      cy.get('#hospital_id_chosen > .chosen-single').click();
      cy.get('#hospital_id_chosen  > div > ul > li:nth-child(2)').click();
  })

  And('I input facility admit date and discharge date for hospitalization information', () => { 
      cy.get('#admit_date').type('12122021');
      cy.get('#discharge_date').type('12122021');
  })

  And('I input surgery and allergy for diagnoses and pre-admission orders', () => { 
      cy.get('#diagnosis_surgery').type('heart failure');
      cy.get('#diagnosis_allergies > .host > .tags > .ng-pristine').type('peanut{enter}');
  })

  And('I select discipline of person completing assessment', () => { 
      cy.get(':nth-child(68) > .oasis__answer > .fg-line > :nth-child(1) > .ng-pristine').click();
  })

  And('I select registered nurse and case manager', () => { 
      cy.get('#cs_rn_chosen > .chosen-single').click();
      cy.get('#cs_rn_chosen > div > ul > li:nth-child(1)').click();//selected REGISTERED NURSE
      cy.get('#cs_cm_chosen > .chosen-single').click();
      cy.get('#cs_cm_chosen > div > ul > li:nth-child(1)').click();//selected CASE MANAGER
  })

  And('I click save', () => { 
      cy.get('.btn__success').click();
  })

  Then('It will display a message that patient is successfully admitted', () => { 
      cy.get('body > div.alert.alert-success.alert-dismissable.growl-animated')
      .contains('Patient has been successfully Admitted')
      .should('be.visible')

  })















