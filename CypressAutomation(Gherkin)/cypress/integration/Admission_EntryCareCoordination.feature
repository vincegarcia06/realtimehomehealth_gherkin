Feature: Automating Entry Form of Coordination

Scenario: Automating Entry Form of Communication Note
    Given I Login
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click Communication
    And I click new

    # Communication Note Summary Start -------------
    And I click Communication Note
    And I input date
    And I input title or subject
    And I input time
    And I input communication notes
    #Notification and Care Coordination
    And I click MD and select MD
    And I click LVN and select LVN
    And I click PT and select PT
    And I click PTA and select PTA
    And I click caregiver and input description
    And I click patient
    And I click MSW and select MSW
    And I click Dietician and select Dietician
    And I click CHHA and select CHHA
    And I click Pharmacy and select Pharmacy
    And I click OT and select OT
    And I click OTA and select OTA
    And I click ST and select ST
    And I click QA Manager and select QA Manager
    #Communicated via
    And I click phone, fax, mail and direct for communicated via
    And I click save button
    # Communication Note Summary End -------------

Scenario: Automating Entry Form of HHA Care Plan
    Given I Login1
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click Communication
    And I click new

    # HHA Care Plan Summary Start -------------
    And I click HHA Care Plan
    # Care Plan Details
    # Vital Signs
    And I click each visit for Temperature, Blood pressure, Pulse or Heart rate, Respiration and Weight
    # Procedures
    And I click each visit for Assist with medications, Assist with elimination, Incontinence care, Record bowel movement and Urinary catheter care
    And I click each visit for Record intake and output, Ostomy care, Empty drainage bag and Inspect or reinforce dressing
    # Personal Care
    And I click each visit for Bed bath, Chair bath, Shower, Tub bath, Hair Shampoo and Hair care or comb hair
    And I click each visit for Oral care or clean dentures, Skin or Foot care, Shave or Groom, Nail care, Perineal care and Assist with dressing
    # Nutrition
    And I click each visit for Meal preparation and Assist with feeding
    And I click limit and input description
    And I click each visit for Fluid intake
    #Activities
    And I click each visit for Reposition or turning in bed
    #Assist w/ mobility/transfer
    And I click Bed, Dangle, Chair, BSC and Shower or Tub
    And I click each visit for Assist with mobility or transfer
    #Assist in ambulation
    And I click Cane, Walker and W-C
    And I click each visit for Assist in ambulation
    #Range of motion
    And I click LUE, RUE, LLE and RLE
    And I click each visit for Range of motion
    #Assist in prescribed exercises per
    And I click POC, PT, OT and ST
    And I click each visit for Assist in prescribed exercises per
    And I click each visit for Equipment care
    #Household Tasks
    And I click each visit for Light housekeeping, Change bed linen, Wash clothes and Assist with errands
    And I input Comments or Additional Instructions
    #Home Health Aide
    And I select Home Health Aide
    And I input date Care Plan reviewed with Home Health Aide
    And I input date Reviewed or Revised
    And I click save button
    # HHA Care Plan Summary End -------------

