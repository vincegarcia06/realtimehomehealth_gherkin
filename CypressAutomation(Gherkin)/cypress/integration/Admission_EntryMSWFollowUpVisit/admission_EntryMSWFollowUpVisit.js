Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });

  const dayjs = require('dayjs')
  
  
  Given('I Login', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Will, Sumera P. Jr.')  //search the added patient 
        cy.wait(5000) 
    });

    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(5000)
    });

    And('I click MSW - Follow-Up Visit', () => {
        cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(17) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click MSW - Follow-Up Visit
        cy.wait(5000)
    });

    And('I input time in and time out', () => {
        cy.get('#timeIn').type('1200') //inputting time in
        cy.get('#timeOut').type('1900') //inputting time out
    });
    And('I input diagnoses', () => {
        cy.get('#diagnosis').type('Test') //inputting diagnoses
    });
    And('I input other diagnoses', () => {
        cy.get('#diagnosisOther').type('Test') //inputting other diagnoses/history
    });
    //HCPCS (click all)
    And('I click all HCPCS', () => {
        cy.get('[rname="q5001"]').click() 
        cy.get('[rname="q5002"]').click() 
        cy.get('[rname="q5009"]').click() 
    });
    //Living Situation
    And('I click unchange from the last visit', () => {
        cy.get(':nth-child(1) > .radio > .ng-pristine').click() //clicking Unchange from the last visit
    });
    And('I click Update to primary caregiver and Change of environment conditional', () => {
        cy.get('[name="upPriCar"]').click() //clicking Update to primary caregiver
        cy.get('[name="chEnvCon"]').click() //clicking Change of environment conditional
    });
    And('I input Psychosocial Update or Note', () => {
        cy.get('#parent > div > form > div > fieldset > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > div > textarea').type('Test') //inputting Psychosocial Update/Note
    });
    //Mental Status (Check all that apply)
    And('I select all that apply for mental status', () => {
        cy.get('[rname="mentalStatus"]').click({multiple:true})
    });
    //Emotional Status (Check all that apply)
    And('I select all that apply for emotional status', () => {
        cy.get('[rname="emoStat"]').click({multiple:true})
        cy.get('[style="width: 8%"] > .checkbox > .ng-scope').click() //clicking other
        cy.get(':nth-child(5) > :nth-child(3) > .fg-line > .global__txtbox').type('Test') //inputting other
    });
    //Visit Goals (click all)
    And('I select all that apply for Visit Goals', () => {
        cy.get('[name="visitGoals"]').click({multiple:true})
        cy.get(':nth-child(4) > [style="width: 20%"] > .checkbox > .ng-scope') //clicking other
        cy.get('#visitGoalsTxt').type('Test') //inputting other
    });
    And('I input visit goal details', () => {
        cy.get(':nth-child(5) > .p-t-5 > .fg-line > .form-control').type('Test') //inputting Visit goal details
    });
    //Visit Interventions (click all)
    And('I select all that apply for Visit Interventions', () => {
        cy.get('[rname="visitInterventions"]').click({multiple:true})
        cy.get(':nth-child(6) > [style="width: 20%"] > .checkbox > .ng-scope') //clicking other
    });
    And('I input visit intervention details', () => {
        cy.get(':nth-child(7) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(2) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(7) > .p-t-5 > .fg-line > .form-control').type('Test') //inputting Intervention details
    });
    //Progress Towards Goals (click all)
    And('I select all that apply for Progress Towards Goals', () => {
        cy.get('[rname="proTowGoa"]').click({multiple:true})
        cy.get(':nth-child(8) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(2) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(5) > td > .checkbox > .ng-scope').click() //clicking good
    });
    And('I input Progress Towards Goals details', () => {
        cy.get(':nth-child(8) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(2) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(7) > .p-t-5 > .fg-line > .form-control').type('Test')
    });
    //Follow-Up Plan (Mark all that apply)
    And('I select all that apply for Follow-Up Plan', () => {
        cy.get('[rname="folUpPlan"]').click({multiple:true})
    });

    And('I click save button', () => {
        cy.get('.btn__success').click() //clicking save button
        cy.wait(10000)
    });



