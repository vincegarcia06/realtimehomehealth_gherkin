Feature: Automating Entry Form of MSW - Follow-Up Visit

Scenario: Automating Entry Form of MSW - Follow-Up Visit
    Given I Login
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click MSW - Follow-Up Visit

    # MSW - Follow-Up Visit Start -------------
    And I input time in and time out
    And I input diagnoses
    And I input other diagnoses
    #HCPCS (click all)
    And I click all HCPCS
    #Living Situation
    And I click unchange from the last visit
    And I click Update to primary caregiver and Change of environment conditional
    And I input Psychosocial Update or Note
    #Mental Status (Check all that apply)
    And I select all that apply for mental status
    #Emotional Status (Check all that apply)
    And I select all that apply for emotional status
    #Visit Goals (click all)
    And I select all that apply for Visit Goals
    And I input visit goal details
    #Visit Interventions (click all)
    And I select all that apply for Visit Interventions
    And I input visit intervention details
    #Progress Towards Goals (click all)
    And I select all that apply for Progress Towards Goals
    And I input Progress Towards Goals details
    #Follow-Up Plan (Mark all that apply)
    And I select all that apply for Follow-Up Plan
    And I click save button
    # MSW - Follow-Up Visit End -------------

