Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });

  const dayjs = require('dayjs')
  
  
Given('I Login', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jendo P. Jr')  //search the added patient 
        cy.wait(10000) 
    });
    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(5000)
    });
    And('I click Communication', () => {
        cy.get(':nth-child(4) > .navtitle').click();//Communication Note
    });
    And('I click new', () => {
        cy.get('.col-sm-12 > .pull-right > .btn').click() //clicking new
    });
    And('I click Communication Note', () => {
        cy.get('[ui-sref="patientcare.commnotesCreate"]').click() //clicking communication note
    });
    And('I input date', () => {
        cy.get('#date').type(dayjs().add(58, 'day').format('MM/DD/YYYY')) ; //inputting date
    });
    And('I input title or subject', () => {
        cy.get('.input-drp > .fg-line > .global__txtbox').type('Laboratory') //inputting title/subject
    });
    And('I input time', () => {
        cy.get(':nth-child(3) > .fg-line > .global__txtbox').type('1200') //inputting time
    });
    And('I input communication notes', () => {
        cy.get('td > .fg-line > .form-control').type('Test') //inputting Communication Notes
    });
    And('I click MD and select MD', () => {
        cy.get(':nth-child(1) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking MD
        cy.get('[style="width: 35%"] > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
        cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > div > div > div > ul > li.active-result.highlighted').click() //clicking result
    });
    And('I click LVN and select LVN', () => {
        cy.get(':nth-child(3) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking LVN
        cy.get(':nth-child(3) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
        cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result
    });
    And('I click PT and select PT', () => {
        cy.get(':nth-child(4) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PT
        cy.get(':nth-child(4) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
        cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result
    });
    And('I click PTA and select PTA', () => {
        cy.get(':nth-child(5) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PTA
        cy.get(':nth-child(5) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
        cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result
    });
    And('I click caregiver and input description', () => {
        cy.get(':nth-child(6) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Caregiver
        cy.get('.p-l-5.ng-isolate-scope > .fg-line > .select > .form-control').type('Test')
    });
    And('I click patient', () => {
        cy.get(':nth-child(7) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Patient
    });
    And('I click MSW and select MSW', () => {
        cy.get(':nth-child(1) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking MSW
        cy.get(':nth-child(1) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
        cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(2)').click() //clicking result
    });
    And('I click Dietician and select Dietician', () => {
        cy.get(':nth-child(2) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Dietician
        cy.get('.inner__table-nb > tbody > :nth-child(2) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
        cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result
    });
    And('I click CHHA and select CHHA', () => {
        cy.get(':nth-child(3) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking CHHA
        cy.get(':nth-child(3) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
        cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > div > div > ul > li').click() //clicking result
    });
    And('I click Pharmacy and select Pharmacy', () => {
        cy.get(':nth-child(4) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Pharmacy
        cy.get(':nth-child(4) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
        cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result
    });
    And('I click OT and select OT', () => {
        cy.get(':nth-child(5) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking OT
        cy.get(':nth-child(5) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
        cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(5) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result
    });
    And('I click OTA and select OTA', () => {
        cy.get(':nth-child(6) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking OTA
        cy.get(':nth-child(6) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
        cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(6) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result
    });
    And('I click ST and select ST', () => {
        cy.get(':nth-child(7) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking ST
        cy.get(':nth-child(7) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
        cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(7) > td:nth-child(4) > div > div > div > div > ul > li').click() //clicking result
    });
    And('I click QA Manager and select QA Manager', () => {
        cy.get(':nth-child(8) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking QA Manager
        cy.get(':nth-child(8) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
        cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(8) > td:nth-child(4) > div > div > div > div > ul > li').click() //clicking result
    });
    And('I click phone, fax, mail and direct for communicated via', () => {
        cy.get('.p-0 > .ng-isolate-scope > .ng-valid').click() //clicking phone
        cy.get(':nth-child(2) > .ng-isolate-scope > .ng-valid').click() //clicking fax
        cy.get(':nth-child(3) > .ng-isolate-scope > .ng-valid').click() //clicking mail
        cy.get(':nth-child(4) > .ng-isolate-scope > .ng-valid').click() //clicking direct
    });
    And('I click save button', () => {
        cy.get('.btn__success').click() // clicking save button
        cy.wait(5000)
    });

Given('I Login1', () => {
    cy.login('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
              cy.viewport(1920, 924);//setting your windows size
    })
  
    And('I visit patient admitted page', () => {
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000)   
    });

    When('I search the patient', () => {
        cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jendo P. Jr')  //search the added patient 
        cy.wait(10000) 
    });
    And('I click the added patient', () => {
        cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
        cy.wait(5000)
    });
    And('I click Communication', () => {
        cy.get(':nth-child(4) > .navtitle').click();//Communication Note
    });
    And('I click new', () => {
        cy.get('.col-sm-12 > .pull-right > .btn').click() //clicking new
    });
    And('I click HHA Care Plan', () => {
        cy.get('[ng-click="createhha()"]').click() //clicking HHA Care Plan
    });
    //Care Plan Details
    //Vital Signs
    And('I click each visit for Temperature, Blood pressure, Pulse or Heart rate, Respiration and Weight', () => {
        cy.get(':nth-child(3) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Temperature
        cy.get(':nth-child(4) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Blood pressure
        cy.get(':nth-child(5) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Pulse/Heart rate
        cy.get(':nth-child(6) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Respiration
        cy.get(':nth-child(7) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Weight
    });
    //Procedures
    And('I click each visit for Assist with medications, Assist with elimination, Incontinence care, Record bowel movement and Urinary catheter care', () => {
        cy.get(':nth-child(9) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Assist with medications
        cy.get(':nth-child(10) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Assist with elimination
        cy.get(':nth-child(11) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Incontinence care
        cy.get(':nth-child(12) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Record bowel movement
        cy.get(':nth-child(13) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Urinary catheter care
    });
    And('I click each visit for Record intake and output, Ostomy care, Empty drainage bag and Inspect or reinforce dressing', () => {
        cy.get(':nth-child(14) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Record intake and output
        cy.get(':nth-child(15) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Ostomy care
        cy.get(':nth-child(16) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Empty drainage bag
        cy.get(':nth-child(17) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Inspect/reinforce dressing
    });
    //Personal Care
    And('I click each visit for Bed bath, Chair bath, Shower, Tub bath, Hair Shampoo and Hair care or comb hair', () => {
        cy.get(':nth-child(3) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Bed bath
        cy.get(':nth-child(4) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Chair bath
        cy.get(':nth-child(5) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Shower
        cy.get(':nth-child(6) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Tub bath
        cy.get(':nth-child(7) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Hair Shampoo
        cy.get(':nth-child(8) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking each visit Hair care/comb hair
    });
    And('I click each visit for Oral care or clean dentures, Skin or Foot care, Shave or Groom, Nail care, Perineal care and Assist with dressing', () => {
        cy.get(':nth-child(9) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Oral care/clean dentures
        cy.get(':nth-child(10) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Skin/Foot care
        cy.get(':nth-child(11) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Shave/Groom
        cy.get(':nth-child(12) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Nail care (Clean/File)
        cy.get(':nth-child(13) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Perineal care
        cy.get(':nth-child(14) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Assist with dressing
    });
    //Nutrition
    And('I click each visit for Meal preparation and Assist with feeding', () => {
        cy.get(':nth-child(16) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Meal preparation
        cy.get(':nth-child(17) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Assist with feeding
    });
    And('I click limit and input description', () => {
        cy.get(':nth-child(2) > td > .radio > .ng-pristine').click() //clicking Limit
        cy.get('td > .fg-line > .global__txtbox').type('Test')
    });
    And('I click each visit for Fluid intake', () => {
        cy.get(':nth-child(18) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Fluid intake
    });
    //Activities
    And('I click each visit for Reposition or turning in bed', () => {
        cy.get(':nth-child(3) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Reposition/turning in bed
    });
    And('I click Bed, Dangle, Chair, BSC and Shower or Tub', () => {
        cy.get(':nth-child(4) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(1) > .checkbox > .p-l-25 > .ng-valid').click() //clicking Bed
        cy.get(':nth-child(4) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(2) > .checkbox > .p-l-25 > .ng-valid').click() //clicking Dangle
        cy.get(':nth-child(4) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(3) > .checkbox > .p-l-25 > .ng-valid').click() //clicking Chair
        cy.get(':nth-child(3) > :nth-child(1) > .checkbox > .p-l-25 > .ng-pristine').click() //clicking BSC
        cy.get(':nth-child(3) > :nth-child(2) > .checkbox > .p-l-25 > .ng-pristine').click() //clicking Shower/Tub
    });
    And('I click each visit for Assist with mobility or transfer', () => {
        cy.get(':nth-child(4) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Assist w/ mobility/transfer
    });
    //Assist in ambulation
    And('I click Cane, Walker and W-C', () => {
        cy.get(':nth-child(7) > table > tbody > :nth-child(2) > :nth-child(1) > .checkbox > .m-r-10 > .ng-valid').click() //clicking Cane
        cy.get(':nth-child(2) > .checkbox > .m-r-10 > .ng-valid').click() //clicking Walker
        cy.get(':nth-child(3) > .checkbox > .m-r-10 > .ng-pristine').click() //clicking W/C
    });
    And('I click each visit for Assist in ambulation', () => {
        cy.get(':nth-child(7) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Assist in ambulation
    });
    //Range of motion
    And('I click LUE, RUE, LLE and RLE', () => {
        cy.get(':nth-child(9) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(1) > .checkbox > .p-l-25 > .ng-valid').click() //clicking lUE
        cy.get(':nth-child(9) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(2) > .checkbox > .p-l-25 > .ng-valid').click() //clicking RUE
        cy.get(':nth-child(9) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(3) > .checkbox > .p-l-25 > .ng-valid').click() //clicking LLE
        cy.get(':nth-child(9) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(4) > .checkbox > .p-l-25 > .ng-valid').click() //clicking RLE
    });
    And('I click each visit for Range of motion', () => {
        cy.get(':nth-child(9) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Range of motion
    });
    //Assist in prescribed exercises per
    And('I click POC, PT, OT and ST', () => {
        cy.get(':nth-child(11) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(1) > .checkbox > .p-l-25 > .ng-valid').click() //clicking POC
        cy.get(':nth-child(11) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(2) > .checkbox > .p-l-25 > .ng-valid').click() //clicking PT
        cy.get(':nth-child(11) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(3) > .checkbox > .p-l-25 > .ng-valid').click() //clicking OT
        cy.get(':nth-child(11) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(4) > .checkbox > .p-l-25 > .ng-valid').click() //clicking ST
    });
    And('I click each visit for Assist in prescribed exercises per', () => {
        cy.get(':nth-child(11) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Assist in prescribed exercises per
    });
    And('I click each visit for Equipment care', () => {
        cy.get(':nth-child(14) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Equipment care

    });
     //Household Tasks
    And('I click each visit for Light housekeeping, Change bed linen, Wash clothes and Assist with errands', () => {
        cy.get(':nth-child(16) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Light housekeeping
        cy.get(':nth-child(17) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Change bed linen
        cy.get(':nth-child(18) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Wash clothes
        cy.get(':nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Assist with errands
    });
    And('I input Comments or Additional Instructions', () => {
        cy.get('.table-input').type('Test') //inputting Comments / Additional Instructions
    });
    //Home Health Aide
    And('I select Home Health Aide', () => {
        cy.get('#aor0005_chosen > .chosen-single').click() //clicking dropdown Home Health Aide
        cy.get('#aor0005_chosen > div > ul').click() //clicking result
    });
    And('I input date Care Plan reviewed with Home Health Aide', () => {
        cy.get(':nth-child(4) > .ng-isolate-scope > #aor0003').type(dayjs().add(58, 'day').format('MM/DD/YYYY')) ; //inputting date Care Plan reviewed with Home Health Aide
    });
    And('I input date Reviewed or Revised', () => {
        cy.get('#aor0006').type(dayjs().add(58, 'day').format('MM/DD/YYYY')) ; //inputting Date Reviewed/Revised
    });
    And('I click save button', () => {
        cy.get('.btn__success').click() // clicking save button
        cy.wait(5000)
    });

