Feature: Automating Entry Form of OASIS - Start of Care SOC

Scenario: Automating Entry Form of Demographics / Clinical Record
    Given I Login
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click OASIS Start of Care
    And I click Edit Button

    # DEMOGRAPHICS/CLINICAL RECORD TAB Start -------------
    And I input time in and time out
    And I input date assessment completed
    And I click No Specific SOC date ordered by physician
    And I input referral date
    And I select early
    And I select all that apply for race or ethnicity
    And I selecting 1 Medicare traditional fee for service for Current Payment Sources for Home Care
    And I select all that apply from which of the following Inpatient Facilities was the patient discharged within the past 14 days
    And I input in patient discharge date
    And I click save button
    # DEMOGRAPHICS/CLINICAL RECORD TAB End -------------

Scenario: Automating Entry Form of Diagnoses / Medical History Tab
    Given I Login1
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click OASIS Start of Care
    And I click Edit Button
    And I click Diagnoses or Medical History Tab

    # DIAGNOSES/MEDICAL HISTORY TAB Start -------------
    And I click Review OASIS Guidelines
    And I select primary Diagnoses
    And I check all that apply for M1028
    And I check all that apply for M1030
    And I check all that apply for M1033
    And I input height and weight
    # Past Medical History Information Section
    And I click all that apply for sensory problems
    And I click all that apply for integumentary problems
    And I click all that apply for endocrine problems
    And I click all that apply for respiratory problems
    And I click all that apply for cardiovascular problems
    And I click all that apply for gastrointestinal problems
    And I click all that apply for genitourinary problems
    And I click all that apply for neurological problems
    And I click all that apply for musculoskeletal problems
    And I click all that apply for circulatory problems
    And I click all that apply for other history problems
    # Surgical history and Hospitalizations
    And I input surgical history and hospitalizations
    # Other Medication Information Section
    And I input exam type, exam date, exam result, contacted last date physician, visit last date physician and reason for contact or visit 
    # Immunizations
    # Influenza vaccine October to March
    And I click yes and input date for influenza vaccine
    And I click agency for influenza vaccine received from
    And I input other and reason for influenza
    And I click agreed for offered and click agency agreed to receive influenza vaccine
    # Pneumonia vaccine (every 5 years)
    And I click yes and input date for pneumonia vaccine
    And I click agency for pneumonia vaccine received from
    And I input other and reason for pneumonia
    And I click agreed for offered and click agency agreed to receive pneumonia vaccine
    # Other immunizations and tests
    And I click yes and input date for shingle test
    And I click yes and input date for TB skin test
    And I click yes and input date for hepatitis B test
    And I click yes and input date for tetanus test
    And I click save button
    # DIAGNOSES/MEDICAL HISTORY TAB End -------------

    Scenario: Automating Entry Form of Vital Signs / Sensory Tab
    Given I Login2
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click OASIS Start of Care
    And I click Edit Button
    And I click Vital Signs or Sensory Tab

    # VITAL SIGNS/SENSORY TAB Start -------------
    And I input temperature and click oral
    And I input pulse or HR and click radial
    And I input respiration
    And I input systolic and diastolic for BP left arm and clicking sitting
    And I input systolic and diastolic for BP right arm and clicking sitting
    And I input systolic and diastolic for BP left leg and clicking sitting
    And I input systolic and diastolic for BP right leg and clicking sitting
    And I input O2 Saturation % on room air
    And I input O2 Saturation % on O2 and input LPM
    And I input blood sugar and clicking fbs
    And I click actual for weight and height
    And Clicking yes for evidence for infection and input description
    And I click MD and CM or Supervisior
    And Clicking yes for NewChangeHold medications and input description
    And I input observation and intervention
    And Clicking 1 for frequency pain
    # Location 1
    And I input pain location
    And I select type
    And I select present level
    And I select worst level
    And I select acceptable level
    And I select level after meds
    #Location2
    And I input pain location1
    And I select type1
    And I select present level1
    And I select worst level1
    And I select acceptable level1
    And I select level after meds1

    And I select all character pain
    And I select all non verbal signs of pain
    And I select all what makes the pain better
    And I select all what makes the pain worse
    And I click yes for pain mediaction and input description for profile
    And I click daily for how often pain meds needed
    And I click effective pain medication effectiveness
    And I click yes for physician aware of pain
    And I input observation and intervention1
    And Clicking 1 for M1200
    # Sensory Status
    # Eyes
    And I click left and right for cataract
    And I click left and right for glaucoma
    And I click left and right for redness
    And I click left and right for pain
    And I click left and right for itching
    And I click left and right for ptsosis
    And I click left and right for sclera reddened
    And I click left and right for edema of eyelids
    And I click left and right for blurred vision
    And I click left and right for blind
    And I click left and right for exudate from eyes
    And I click left and right for excessive tearing
    And I click left and right for macular degeneration
    And I click left and right for retinopathy
    And I input other and click left and right for other status
    # Ears
    And I click left and right for HOH
    And I click left and right for Hearing aid
    And I click left and right for Deaf
    And I click left and right for Tinnitus
    And I click left and right for Drainage
    And I click left and right for Pain for ears
    And I input other and click vertigo
    # Mouth
    And I click dentures and click upper, lower and partial
    And Clicking poor dentition, gingivitis, toothache and loss of taste for Mouth
    And I click lesions and input description for lesion
    And I click mass or tumor and input description for mass or tumor
    And I click difficulty of chewing or swallowing
    And I input others for mouth
    # Nose
    And I select all for nose
    And I input lesions for nose
    And I input others for nose
    # Throat
    And I select all for throat
    And I input lesions for throat
    And I input others for throat
    # Speech
    And I select all for Speech
    And I input others for Speech
    # Touch
    And I select all for Touch
    And I input others for Touch

    And I input observation and intervention2
    And I click save button

Scenario: Automating Entry Form of Integumentary / Endocrine Tab
    Given I Login3
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click OASIS Start of Care
    And I click Edit Button
    And I click Integumentary or Endocrine Tab

    # INTEGUMENTARY/ENDOCRINE TAB Start -------------
    And I click pink for skin color and input other
    And I click warm for skin temp
    And I click dry for moisture and input other
    And I click normal for turgor

    #Skin Integrity
    And I click skin intact, lesion and add lesion icon
    And I select one lesion
    And I input location and comment
    And I click wound and add wound icon
    And I input wound, location and comment
    And I input other for skin integrity
    And I input observation and intervention

    #Braden Scale for Predicting Pressure Sore Risk
    And I click 1 for sensory perception
    And I click 1 for moisture
    And I click 1 for activity
    And I click 1 for mobility
    And I click 1 for nutrition
    And I click 2 for friction and shear
    And I click yes for M1306
    #(M1311) - Current Number of Unhealed Pressure Ulcers/Injuries at Each Stage
    And I input A1, B1, C1, D1, E1, F1
    #(M1322) - Current Number of Stage 1 Pressure Injuries
    And I click 1 for M1322
    #(M1324) - Stage of Most Problematic Unhealed Pressure Ulcer/Injury that is Stageable
    And I click 2 for M1324
    #(M1330) - Does this patient have a Stasis Ulcer?
    And I click 2 for M1330
    #(M1332) - Current Number of Stasis Ulcer(s) that are Observable
    And I click 2 for M1332
    #(M1334) - Status of Most Problematic Stasis Ulcer that is Observable
    And I click 2 for M1334
    #(M1340) - Does this patient have a Surgical Wound?
    And I click 1 for M1340
    #(M1342) - Status of Most Problematic Surgical Wound that is Observable
    And I click 2 for M1342

    #Endocrine System
    And I select all for endocrine system
    And I click save button
    And modal will display
    And I click access later button
    # INTEGUMENTARY/ENDOCRINE TAB End -------------

Scenario: Automating Entry Form of Cardiopulmonary Tab
    Given I Login4
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click OASIS Start of Care
    And I click Edit Button
    And I click Cardiopulmonary Tab

    # CARDIOPULMONARY TAB Start -------------
    And I click 1 for M1400
    And I click left, right, anterior, posterior, upper, middle, lower for diminished
    And I click left, right, anterior, posterior, upper, middle, lower for absent
    And I click left, right, anterior, posterior, upper, middle, lower for rales crackles
    And I click left, right, anterior, posterior, upper, middle, lower for rhonchi
    And I click left, right, anterior, posterior, upper, middle, lower for wheeze
    And I click left, right, anterior, posterior, upper, middle, lower for stridor
    And I click yes for abnormal breathing patterns
    #Abnormal breathing patterns
    And I select all abnormal breathing patterns
    #Cough
    And I click yes for cough
    And I click productive for character
    And I click white for sputum color
    And I click thin for sputum character
    And I click small for sputum amount
    #Special Procedure (Select All)
    And I select all special procedure
    And I input observation and intervention
    #Oxygen Risk Assessment (All clicking yes)
    And I click yes for oxygen risk assessment
    #Are there potential sources of open flames identified? (select all)
    And I click all potential sources of open flames
    And I input observation and intervention1
    #Oxygen Theraphy
    And I click continuous for type
    #Oxygen delivery (select all)
    And I select all oxygen delivery
    And I input liters or minute
    #Oxygen source (select all)
    And I selecy all oxygen source
    And I click yes for backup O2 tank
    And I click vendor notified
    And I input observation and intervention2
    #Other form for Tracheostomy
    And I inpput brand, trach tube change, date last changed and inner cannula
    And I input observation and intervention3
    #Other form for BiPAP/CPAP
    And I input brand for BiPAP or CPAP
    And I click yes for Device working properly and Compliant with use of device
    And I input observation and intervention4
    #Other form for Suctioning
    And I click oral
    And I click yes for Is the person performing the suctioning proficient
    And I click yes for Is the suction equipment setup always ready for use
    #Suction procedure done by (select all)
    And I select all for suction procedure done by
    And I input observation and intervention5
    #Other form for Ventilator
    And I input brand, ridal volume, FiO2 and assist control for ventilator
    And I input PEEP, SIMV, pressure control and PRVC for ventilator
    And I input observation and intervention6
    #Other form for PleurX
    And I input date catheter inserted
    And I click daily and input other for drainage frequency
    And I input ml for amount drained
    And I click done
    #Procedure done by (select all)
    And I select all for procedure done by
    And I input observation and intervention7
    #Cardiovascular
    And I click regular for heart rhythm
    And I click least than three for capillary refill
    And I click yes for JVD, peripheral edema, chest pain and cardiac device
    And I select cardiac device
    And I click yes and input other fow weight gain
    #Pulses
    And I click pedal left and right for bounding
    And I click popliteal left and right for bounding
    And I click femoral left and right for bounding
    And I click brachial left and right for bounding
    And I click radial left and right for bounding
    And I input observation and intervention8
    #Peripheral edema
    And Clicking +1 for Pedal edema - Left
    And Clicking +1 for Pedal edema - Right
    And Clicking +1 for Ankle edema - Left
    And Clicking +1 for Ankle edema - Right
    And Clicking +1 for Leg edema - Left
    And Clicking +1 for Leg edema - Right
    And Clicking +1 for Sacral edema	
    And Clicking +1 for Generalized edema
    And I input observation and intervention9
    #Chest Pain
    #Character (select all)
    And I select all character
    And I click left for radiating to shoulder, jaw, neck, arm
    And I select all for accompanied by
    And I input frequency and duration of pain
    And I input observation and intervention10
    #Pace Maker
    And I input brand, rate setting, date implanted and date last tested
    And I click no for
    And I input observation and intervention11
    And I click save button
    # CARDIOPULMANRY TAB End -------------

Scenario: Automating Entry Form of Nutrition/Elimination Tab
    Given I Login5
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click OASIS Start of Care
    And I click Edit Button
    And I click Nutrition or Elimination Tab

    # NUTRITION / ELIMINATION TAB Start -------------
    And I click soft and input inches in girt for abdomen
    And I click active for bowel sounds
    And I click good for appettite
    And I select all other Symptoms
    And I input observation and intervention
    And I click projectile for type
    And I click small for amount
    And I click watery and input other for character
    And I input frequency
    And I input observation and intervention1
    #Nutrition/Diet (select all)
    And I select all nutrition or diet
    And I input ml for fluid restriction and other nutrition or diet
    And I click all yes for nutrition health screen
    And I click decubitus, ulcer, burn and wound
    #Enteral Nutrition
    And I click dobhoff for feeding via
    And I input tube insertion date
    And I click pump for formula delivery system
    And I input amount and ml for feeding formula
    And I input amount and ml for liquid supplement
    And I input ml and hours per day for pump rate for hours
    And I click open system for enteral feeding system
    And I input hours if residual volume over for hold feeling for
    And I input ml for hold feeling for
    And I input ml for Gastric residual amount
    And I click yes for Tolerating feedings well and NPO
    And I input NPO
    And I select all for Ostomy care or feedings by
    And I input observation and intervention2
    #Genitourinary Status
    #Urine clarity (select all except clear)
    And I select all urine clarity except clear
    And I click straw and input other for urine color
    And I click yes and input description for urine odor
    And I click yes for abnormal elimination
    And I select all for abnormal elimination
    #Special procedures
    And I select all for special procedure
    And I input observation and intervention3
    #Indwelling catheter
    And I click urethral for catheter type
    And I click 14 for catheter size
    And I click 5 for balloon inflation
    And I click 2-way for catheter lumens
    And I input day for catheter change
    And I input date last change
    And I click done for catheter change
    And I click bedside and leg bag for drainage bag
    And I input days for MD ordered irrigation frequency
    And I click as needed, done and none for MD-ordered irrigation frequency
    And I input amount for MD-ordered irrigation solution
    And I input ml and click none for MD-ordered irrigation solution
    And I input observation and intervention4
    #Intermittent Catheterization
    And I input frequency for Intermittent Catheterization
    And I select all for done by
    And I input observation and intervention5
    #Nephrostomy
    And I input days and click as needed and done for Nephrostomy dressing change
    And I input days and click as needed and done for Nephrostomy bag change
    And I input days and click as needed and done for MD-ordered irrigation frequency
    And I input amount and ml and click none for MD-ordered Irrigation solution
    And I input observation and intervention6
    And I input days for Urostomy pouch change frequency
    And I input observation and intervention7
    And I click daily for change frequency
    And I input other in change frequency
    And I select all drainage bag
    And I input observation and intervention8
    #Hemodialysis
    And I click AV shunt and input location for AV access
    And I click permacath and input other for AV access
    And I click yes for Bruit present and Thrill strong
    And I click all dialysis schedule
    And I input observation and intervention9
    #Peritoneal Dialysis
    And I click Continuous Ambulatory Peritoneal Dialysis CAPD for type
    And I input APD machine
    And I select all for dialysate
    And I input dwell time and hours
    And I select all for peritoneal dialysis done by
    And I input observation and intervention10
    And I click 1 for M1600
    And I click 1 for M1610
    #Lower GI Status
    And I select all for bowel movement
    And I click soft and input other for stool character
    And I click yellow or brown and input other for stool color
    And I click effective and click and input MD notified
    And I click as needed and input other for laxative or Enema
    And I input observation and intervention11
    #Lower GI Ostomy
    And I click colostomy for ostomy type
    And I input cm for stoma diameter
    And I click healed for ostomy wound
    And I select all for care done by for Lower GI Ostomy
    And I input observation and intervention12
    And I click 0 for M1620
    And I click 0 for M1630
    And I click save button


Scenario: Automating Entry Form of Neurologic / Behavioral Tab
    Given I Login6
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click OASIS Start of Care
    And I click Edit Button
    And I click Neurologic or Behavioral Tab

    # NEUROLOGIC/BEHAVIORAL TAB Start -------------
    And I click left greater than right for size manual
    And I click left for non-reactive
    #Mental status (select all)
    And I select all for mental status
    And I click adequate MD notified for sleep or rest
    #Hand grips
    And I click left and right for strong
    And I click left and right for weak
    #Other signs (select all)
    And I select all for other signs
    #Weakness
    And I click left and right for upper extremity Weakness
    And I click left and right for lower extremity Weakness
    And I click left for hemiparesis
    #Paralysis
    And I click left for hemiplegia and paraplegia
    #Tremors
    And I click left, right and fine for upper extremity Tremors
    And I click left, right and fine for lower extremity Tremors
    #Seizure
    And I click grand mal
    And I input date of last Seizure
    And I input duration
    And I input observation and intervention
    And I click 1 for M1700
    And I click 1 for M1710
    And I click 2 for M1720
    And I click 1 for M1730
    And I click for A and B for M1730
    And I click 2 for M1740
    And I click 2 for M1745
    # Knowledge Deficit (select all)
    And I select all for Knowledge Deficit
    # Thought Process, Affect and Behavioral Status (select all)
    And I select all for Thought Process, Affect and Behavioral Status
    #Psychosocial Factors
    And I click yes for learning barrier
    And I click patient and caregiver for language barrier
    And I input speak
    And I input Interpreter needed, Sign language Type, Interpreter service provider and Interpreter contact number
    #Learning barrier
    And I click yes learning barrier
    And I click unable to read
    And I click functional and input description
    And I click physical and input description
    And I click Mental or Cognitive and input description
    And I click Psychosocial and input description
    #Signs of abuse, neglect
    And I click yes for Signs of abuse, neglect
    And I click potential
    And I click physical and input description Signs of abuse, neglect
    And I click verbal and input description
    And I click referral to MSW and input description
    And I click Referred to Child or Adult Protective Services
    # Spiritual/cultural issues
    And I clicking yes for Spiritual or cultural issues
    And I input implications that may impact care
    And I input spiritual resource, contact person and phone
    And I input observation and intervention1
    And I click save button
    # NEUROLOGIC/BEHAVIORAL TAB End -------------


Scenario: Automating Entry Form of ADL / IADL / Musculoskeletal Tab
    Given I Login7
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click OASIS Start of Care
    And I click Edit Button
    And I click ADL or IADL or Musculoskeletal Tab

    # ADL/IADL/Musculoskeletal TAB Start -------------
    #Musculoskeletal Status
    And I click strong for muscle strength
    And I click limited for Range of motion
    # limited (select all)
    And I select all for limited
    And I click independent for Bed mobility
    And I click independent for Transfer ability
    And I click steady for Gait or Ambulation
    And I click good for balance
    And I input seconds for Timed Up & Go
    And I click practiced once before actual test and unable to perform for Timed Up & Go
    And I click low for risk for falls
    And I click left and right for amputation
    And I click BK, AK, UE and input other for amputation
    And I click new, input location and click cast for fracture
    And I input observation and intervention
    #If cast is present, assessment of extremity distal to cast
    And I click pink for color
    And I click strong for Pulses
    And I click < 3 sec for Capillary refill
    And I click warm for temperature
    And I click normal for sensation
    And I click able to move for motor function
    And I click yes for dwelling
    And I input intervention
    # Functional Limitations (select all)\
    And I select all functional Limitations
    # Activities Permitted (select all)
    And I select all activities Permitted
    #MAHC - 10 - Fall Risk Assessment Tool (check all points)
    And I check all points for MAHC - 10 - Fall Risk Assessment Tool
    And I click 1 for M1800 - Grooming
    And I click 1 for M1810 - Current Ability to Dress Upper Body
    And I click 1 for M1820 - Current Ability to Dress Lower Body
    And I click 1 for M1830 - Bathing
    And I click 1 for M1840 - Toilet Transferring
    And I click 1 for M1845 - Toileting Hygiene
    And I click 1 for M1850 - Transferring
    And I click 1 for M1860 - Ambulation or Locomotion
    And I click 1 for M1870 - Feeding or Eating
    And I click 1 for M1910 - Has this patient had a multi-factor Falls Risk Assessment using a standardized, validated assessment tool
    #Section GG Functional Abilities and Goals
    #(GG0100) - Prior Functioning
    And I select 3 for A - Self Care
    And I select 3 for B - Indoor Mobility - Ambulation
    And I select 3 for C - Stairs
    And I select 3 for D - Functional cognition
    #(GG0110). Prior Device Use (check all)
    And I check all prior device use
    #GG0130. Self‐Care
    And I select 5 on A - Eating for SOC or ROC Performance
    And I select 6 on A - Eating for Discharge Goal
    And I select 5 on B - Oral Hygiene for SOC or ROC Performance
    And I select 6 on B - Oral Hygiene for Discharge Goal
    And I select 5 on C - Toileting Hygiene for SOC or ROC Performance
    And I select 6 on C - Toileting Hygiene for Discharge Goal
    And I select 5 on E - Shower or bathe self for SOC or ROC Performance
    And I select 6 on E - Shower or bathe self for Discharge Goal
    And I select 5 on F - Upper body dressing for SOC or ROC Performance
    And I select 6 on F - Upper body dressing for Discharge Goal
    And I select 5 on G - Lower body dressing for SOC or ROC Performance
    And I select 6 on G - Lower body dressing for Discharge Goal
    And I select 5 on H - Putting on or taking off footwear for SOC or ROC Performance
    And I select 6 on H - Putting on or taking off footwear for Discharge Goal
    #GG0170. Mobility
    And I select 5 on A - Roll left and right for SOC or ROC Performance
    And I select 5 on A - Roll left and right for Discharge Goal
    And I select 5 on B - sit to lying for SOC or ROC Performance
    And I select 5 on B - sit to lying for Discharge Goal
    And I select 6 on C - lying to sitting on side of bed for SOC or ROC Performance
    And I select 6 on C - lying to sitting on side on bed for Discharge Goal
    And I select 5 on D - sit to stand for SOC or ROC Performance
    And I select 5 on D - sit to stand for Discharge Goal
    And I select 5 on E - Chair or bed-to-chair transfer for SOC or ROC Performance
    And I select 5 on E - Chair or bed-to-chair transfer for Discharge Goal
    And I select 5 on F - toilet transfer for SOC or ROC Performance
    And I select 5 on F - toilet transfer for Discharge Goal
    And I select 5 on G - Car Transfer for SOC or ROC Performance
    And I select 5 on G - Car Transfer for Discharge Goal
    And I select 5 on I - Walk ten feet for SOC or ROC Performance
    And I select 5 on I - Walk ten feet for Discharge Goal
    And I select 5 on J - walk fifty feet with two turns for SOC or ROC Performance
    And I select 5 on J - walk fifty feet with two turns for Discharge Goal
    And I select 5 on K - walk one-fifty feet for SOC or ROC Performance
    And I select 5 on K - walk one-fifty feet for Discharge Goal
    And I select 5 on L - walking ten feet on uneven surfaces for SOC or ROC Performance
    And I select 5 on L - walking ten feet on uneven surfaces for Discharge Goal
    And I select 6 on M - one step - curb for SOC or ROC Performance
    And I select 5 on M - one step - curb for Discharge Goal
    And I select 5 on N - four steps for SOC or ROC Performance
    And I select 5 on N - four steps for Discharge Goal
    And I select 5 on O - twelve steps for SOC or ROC Performance
    And I select 5 on O - twelve steps for Discharge Goal
    And I select 5 on P - picking up object for SOC or ROC Performance
    And I select 5 on P - picking up object for Discharge Goal
    And I 1-yes on Q - Does patient use wheelchair and or scooter
    And I select 6 on R - wheel fifty feet with two turns for SOC or ROC Performance
    And I select 6 on R - wheel fifty feet with two turns for Discharge Goal
    And I 1-manual on RR1 - Type of wheelchair
    And I select 5 on S - wheel one-fifty feet for SOC or ROC Performance
    And I select 5 on S - wheel one-fifty feet for Discharge Goal
    And I 1-manual on SS1 - Type of wheelchair
    And I click save button
    # ADL/IADL/Musculoskeletal TAB End -------------

Scenario: Automating Entry Form of Medication Tab
    Given I Login8
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click OASIS Start of Care
    And I click Edit Button
    And I click Medication Tab

    # MEDICATION TAB Start -------------
    #High-risk medications patient is currently taking (select all)
    And I select all High-risk medications patient is currently taking
    #Home medication management (select all)
    And I select all home medication management
    And I click yes for Medication discrepancy noted during this visit
    And I click yes for Oral medications -tablets-capsules- prepared in a pill box
    And I click yes for Use of medication schedule in taking medications
    And I input observation and intervention
    And I click 1 for M2001 - Drug Regimen Review
    And I click 1 for M2003 - Medication Follow-up
    And I click 1 for M2010 - Patient or Caregiver High-risk Drug Education
    And I click 1 for M2020 - Management of Oral Medications
    And I click 1 for M2030 - Management of Injectable Medications
    And I click Injectable medication administered
    #Form for Injectable medication(s) administered
    And I input administered
    And I click intramuscular
    And I input site
    And I input observation and intervention2
    And I click Venous Access and IV Therapy
    # Form for Venous Access and IV Therapy
    # Access (select all)
    And I select all for access
    And I select all for purpose
    #Peripheral Venous Access
    And I click short and input other for catheter type
    And I click 24 and input other for catheter gauge
    And I click left and arm and input other for insertion site
    And I input insertion date
    And I input days and click done for site change
    And I click site condition and input WNL for observation
    And I input intervention
    #Midline Catheter
    And I click 8 cm and input other for catheter length
    And I click 22 and input other for catheter gauge
    And I click left arm and input other for insertion site
    And I input date inserted
    And I input days and click done for dressing change
    And I click site condition and input WNL for observation1
    And I input intervention1
    # Central Venous Access
    And I click PICC line and input other for CVAD catheter
    And I click single for no. of lumens
    And I click peripheral for insertion site
    And I input date inserted1
    And I input days and click done for dressing change1
    And I input flushing solution
    And I input days and click done for flush frequency
    And I click site condition and input WNL for observation2
    And I input intervention2
    # Implanted Port
    And I click portacath and input other for port device
    And I click single chamber for port reservoir
    And I click 0.50 for huber needle
    And I click 19 for huber gauge
    And I input flushing solution1
    And I input days and click done for flush frequency1
    And I click site condition and input WNL for observation3
    And I input intervention3
    #Intravenous Therapy
    And I input IV medication
    And I click add icon for IV medication
    And I input new IV medication
    And I input IV fluid theraphy
    And I click add icon for fluid theraphy
    And I input new fluid theraphy
    And I click IV drip, input other and click done for administer via
    And I input flush procedure
    And I input pre-infusion solution
    And I input post-infusion solution
    And I click Patient tolerated IV therapy well with no S-S of adverse reactions for observation
    And I input observation and intervention1
    And I click save button
    # MEDICATION TAB End -------------

Scenario: Automating Entry Form of Care Management Tab
    Given I Login9
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click OASIS Start of Care
    And I click Edit Button
    And I click Care Management Tab

    # CARE MANAGEMENT TAB Start -------------
    And I click caregiver always available and reliable for caregiving status
    And I input caregiver name, relationship, phone, name of facility, phone for facility, contact person and community resources utilized
    And I click 01 around the clock - patient lives alone for M1100
    And I click 1 for M2102 - Types and Sources of Assistance
    And I input M2200 - Therapy Need
    #Physician Orders for Disciplines and Ancillary Referrals
    #Discipline (select all)
    And I select all Discipline
    And I input all for frequency
    And I input Other MD Referral Orders
    # Physical Therapy (select all)
    And I select all physical theraphy
    # Occupational Therapy (select all)
    And I select all occupational theraphy
    # Speech Therapy/SLP (select all)
    And I select all speech theraphy or SLP
    # MSW (select all)
    And I select all for MSW
    #CHHA
    And I click assist personal care and input other for CHHA
    #Dietician
    And I click identified high risk from nutritional screening initiative and input other for Dietician
    #DME
    And I click has for walker, cane, and wheelchair, bedside commode, shower bench, feeding pump,
    And I click has for bedside commode, shower bench and feeding pump
    And I click has for suction machine, O2 concentrator and nebulizer machine
    And I click has for hospital bed, low air loss mattress and input other for DME
    And I click yes for DME working properly
    And I click malfunction reported to DME vendor
    #Reasons for Home Health (Mark all that apply)
    And I select all reasons for home health
    And I input additional reasons for home health
    #Homebound Status (Must meet the two criteria below)
    And I click has a condition such that leaving his or her home is medically contraindicated for criterion one
    And I click there must exist a normal inability to leave home
    #Conditions that support the additional requirements in Criterion Two (select all)
    And I select all Conditions that support the additional requirements in Criterion Two
    #Safety Measures (select all)
    And I select all Safety Measures
    And I input additional Safety Measures
    #Home Environment Safety (select all)
    And I select all Home Environment Safety
    And I input observation and intervention
    #Emergency Planning/Fire Safety
    And I click yes for Smoke detectors on all levels of home
    And I click yes for Smoke detectors tested and functioning
    And I click yes for Fire extinguisher
    And I click yes for More than one exit
    And I click yes for Plan for exit
    And I click yes for Plan for power failure
    And I click yes for Carbon monoxide detector
    And I click SN to assist patient to obtain ERS button
    And I click SN to develop individualized emergency plan for patient
    And I input comment
    #Advance Directives
    And I click yes for Does patient have an Advanced Directive
    And I click yes for Is the patient DNR - Do Not Resuscitate
    And I click yes for Is the patient DNI - Do Not Intubate
    And I click yes for Does the patient have a Living Will
    And I click yes for Does patient have a Medical Power of Attorney
    And I input name
    And I click yes for Has a surrogate
    And I click yes for Are copies on file with the Agency
    And I click yes for Is patient provided with written and verbal information on the subject
    #Patient Classification Risk Levels
    And I click level 1 - high priority
    And I click poor for prognosis
    #SKILLED INTERVENTIONS
    And I input Skilled Assessment and Observation
    And I input Skilled Teaching and Instructions
    #Patient/Caregiver Response to Skilled Interventions
    #For Patient
    And I click full for verbalized understanding - patient
    And I click for for return demonstration Performance - patient
    And I click yes for Need follow-up teaching or instruction - patient
    #For Caregiver
    And I click full for verbalized understanding - caregiver
    And I click for for return demonstration Performance - caregiver
    And I click yes for Need follow-up teaching or instruction - caregiver
    And I input comment1
    #Care Coordination
    And I select physician contacted via
    And I input date, hour, regarding, Additional MD orders and patient-s next physician visit
    #Case conference with (select all)
    And I select all case conference
    And I input other for case conference
    And I click EMR for via
    And I input date and hour
    And I click yes for first visit
    And I click CA identification card
    And I click medicare card
    And I click CA driver-s license
    And I input other
    And I input Plan for next visit
    #Discharge Plans (select all)
    And I select all discharge plan
    And I click patient, PCG and other
    And I input description for other
    And I click agreeable
    And I click good for rehabilitation potential
    And I input additional discharge plans
    And I input patient goals
    And I click save button
    # CARE MANAGEMENT TAB End -------------








 