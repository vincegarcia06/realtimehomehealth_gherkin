Feature: Automating Entry Form of MSW Assessment

Scenario: Automating Entry Form of MSW Assessment
    Given I Login
    And I visit patient admitted page
    When I search the patient
    And I click the added patient
    And I click MSW Assessment

    # MSW Assessment Start -------------
    And I input time in and time out
    #Evaluation and Plan of Care
    #Plan of Care/Medical Social Work Orders(click all)
    And I select all for Plan of Care or Medical Social Work Orders
    And I click crisis intervention to
    #Order Crises(click all)
    And I click all order crisis
    And I input Home Setting or Environment
    And I input Emergency Contact
    And I input Support System
    And I input Homebound Status
    #Mental Status / Behavior (click all)
    And I click all Mental Status or Behavior
    #Financial Status (click all)
    And I click all Financial Status
    And I input Clinical Findings / Problems
    And I input Goals
    And I input Interventions
    And I input Referrals
    And I input Outcome & Plan
    #Frequency
    And I click 1 Visit x 60 days
    And I click and input other
    #Discharge Summary
    #Reason for Discharge (click all)
    And I click all Reason for Discharge
    #Overall Status of Patient at Discharge
    And I click all Overall Status of Patient at Discharge
    #Patient's Functional Status ADL's
    And I click independent for Patients Functional Status ADLs
    #Patient's Functional Status IADL's
    And I click independent for Patients Functional Status IADLs
    And I input Continuing Level of In-Home Care
    And I input Problems Identified
    And I input Status of Problems at Discharge
    And I input Summary of Care Provided
    And I click goals met
    And I click save button

    # MSW Assessment End -------------

